class Occupation < ActiveRecord::Base
  attr_accessible :description, :kind, :month_finish, :month_start, :name, :year_finish, :year_start, :user_id, :place_id
  belongs_to :user
  belongs_to :place

  KINDS = { 'Работа'=> '0', 'ВУЗ'=> '1', 'Школа'=> '2' }
  MONTH = { 'Январь'=> 1, 'Февраль'=> 2, 'Март'=> 3, 'Апрель'=> 4, 'Май'=> 5, 'Июнь'=> 6,
            'Июль'=> 7, 'Август'=> 8, 'Сентябрь'=> 9, 'Октябрь'=> 10, 'Ноябрь'=> 11, 'Декабрь'=> 12 }
  MONTHR = { 'Января'=> 1, 'Февраля'=> 2, 'Марта'=> 3, 'Апреля'=> 4, 'Мая'=> 5, 'Июня'=> 6,
            'Июля'=> 7, 'Августа'=> 8, 'Сентября'=> 9, 'Октября'=> 10, 'Ноября'=> 11, 'Декабря'=> 12 }
  YEAR =* 1930..(Date.today.year)



  validates :kind, inclusion: { in: KINDS.values }
  validates :month_finish, inclusion: { in: MONTH.values }, :allow_nil => true
  validates :month_start, inclusion: { in: MONTH.values }, :allow_nil => true
  validates :year_start, inclusion: { in: YEAR }, :allow_nil => true
  validates :year_finish, inclusion: { in: YEAR }, :allow_nil => true




end
