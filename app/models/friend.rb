class Friend < ActiveRecord::Base
  attr_accessible :friend_id, :status, :user_id

  belongs_to :user
  belongs_to :friend, class_name: 'User'


  after_initialize :default_values

  def default_values
    self.status ||= 0
  end


end
