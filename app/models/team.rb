class Team < Community

  def self.model_name
    Community.model_name
  end

  def finished_events
    Event.where('(events.team1_id = ? OR events.team1_id = ?) AND events.date < ?', self.id, self.id, Time.now).order(date: :asc)
  end

  def waiting_events
    Event.where('(events.team1_id = ? OR events.team1_id = ?) AND events.date > ?', self.id, self.id, Time.now).order(date: :asc)
  end

  def self.search(params)
    teams = self
    if params[:q].present?
      teams = teams.where('about LIKE ? OR name LIKE ?', "%#{params[:q]}%", "%#{params[:q]}%")
    end
    if params[:name].present?
      teams = teams.where('name LIKE ?', "%#{params[:name]}%")
    end
    if params[:sport_type].present?
      teams = teams.where('sport_type_id = ?', params[:sport_type])
    end
    if params[:place].present?
      place = Place.find(params[:place])
      places_ids = place.self_and_descendants.pluck(:id)
      teams = teams.where('place_id IN(?)', places_ids)
    end
    if params[:search_player].present?
      search_teams = TeamPlayerSearch
      if params[:sport_type_id].present?
        root_sport_type = SportType.find(params[:sport_type_id])
        sport_types_ids = root_sport_type.self_and_descendants.pluck(:id)
        search_teams = search_teams.where('sport_type_id IN(?)', sport_types_ids)
      end
      if params[:level].present?
        search_teams = search_teams.where(level: params[:level])
      end
      teams = teams.where('id IN (?)', search_teams.all.map(&:community_id))
    end
    teams
  end

end