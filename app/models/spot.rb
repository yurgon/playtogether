class Spot < ActiveRecord::Base
  attr_accessible :about, :kind, :name, :paid, :place_id, :size, :user_id, :address

  belongs_to :place
  belongs_to :user

  validates_presence_of :name

  has_many :comments, as: :commentable, order: 'date DESC', dependent: :destroy
  has_many :photoalbums, :as => :photoalbumable, order: 'created_at DESC'
  has_many :videoalbums, :as => :videoalbumable, order: 'created_at DESC'

  KINDS = { 'Открытая'=> 0, 'Крытая'=> 1 }
  PAIDS = { 'Бесплатная'=>0, 'Платная'=> 1 }


  has_many :favorites, as: :favoritable, dependent: :destroy

  def fullname
    self.name
  end


  include ActionView::Helpers

  def full_description
    truncate(self.about, length: 300)
  end



  def self.search(params)
    teams = self
    if params[:q].present?
      teams = teams.where('about LIKE ? OR name LIKE ?', "%#{params[:q]}%", "%#{params[:q]}%")
    end
    if params[:name].present?
      teams = teams.where('name LIKE ?', "%#{params[:name]}%")
    end
    if params[:place].present?
      place = Place.find(params[:place])
      places_ids = place.self_and_descendants.pluck(:id)
      teams = teams.where('place_id IN(?)', places_ids)
    end

    teams
  end

end
