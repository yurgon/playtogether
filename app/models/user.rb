class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable # :confirmable
  # attr_accessible :title, :body

  attr_accessible :email, :password, :password_confirmation
  attr_accessible :terms, :remember_me
  validates_acceptance_of :terms, message: 'Необходимо подтвердить согласие с условиями регистрации', if: :new_record?

  attr_accessible :fname, :lname, :nickname, :sex, :birthday_day, :birthday_month, :birthday_year, :email2, :phone, :about, :about_sport, :avatar, :place_id, :cover_page
  validates_presence_of :fname, :lname, :nickname
  validates :nickname, uniqueness: true
  has_many :comments, as: :commentable, order: 'date DESC', dependent: :destroy

  has_many :friends, order: 'status DESC'

  has_many :friends_users, :source => :friend, through: :friends

  has_many :reverse_friends, class_name: 'Friend', foreign_key: 'friend_id'

  has_many :reverse_friends_users, :source => :user_id, through: :reverse_friends

  has_many :photoalbums, :as => :photoalbumable, order: 'created_at DESC'
  has_many :videoalbums, :as => :videoalbumable, order: 'created_at DESC'

  has_many :memberships, dependent: :destroy
  has_many :communities, through: :memberships

  has_many :community_roles, dependent: :destroy
  has_many :communities_with_roles, through: :community_roles, :source => :community

  has_many :event_subscribes
  has_many :accepted_event_members

  has_many :occupations, order: 'year_start DESC, month_start DESC', dependent: :destroy
  accepts_nested_attributes_for :occupations, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true
  attr_accessible :occupations_attributes
  has_many :members_requests

  mount_uploader :avatar, AvatarUploader
  mount_uploader :cover_page, CoverPageUploader

  validates :fname, :lname, :presence => true

  belongs_to :place

  has_many :users_sport_types, dependent: :destroy
  accepts_nested_attributes_for :users_sport_types, :reject_if => lambda { |a| a[:sport_type_id].blank? }, :allow_destroy => true
  attr_accessible :users_sport_types_attributes


  attr_accessible :permission_send_message,
                  :permission_view_profile,
                  :permission_view_friends,
                  :permission_view_wall,
                  :permission_view_photo,
                  :permission_view_video,
                  :permission_comment_photo,
                  :permission_comment_video,
                  :permission_comment_wall

  def is_friend?(user)
    Friend.where(user_id: self.id, friend_id: user.id, status: 2).first.nil?
  end

  def name
    (self.fname+' '+self.lname).strip
  end

  def self.search(params)
    users = self

    if params[:q].present?
      params[:q].to_s.strip!
      users = users.where('about LIKE :q OR about_sport LIKE :q OR fname LIKE :q OR lname LIKE :q OR nickname LIKE :q', {:q => params[:q]})
    end

    if params[:lname].present?
      params[:lname].to_s.strip!
      users = users.where('lname LIKE :q', {:q => params[:lname]})
    end

    if params[:fname].present?
      params[:fname].to_s.strip!
      users = users.where('fname LIKE :q', {:q => params[:fname]})
    end

    if params[:nickname].present?
      params[:nickname].to_s.strip!
      users = users.where('nickname LIKE :q', {:q => params[:nickname]})
    end

    search_teams = false

    if params[:search_team].present?
      search_teams = UsersSportType.where(search_team: true)
    end

    if params[:sport_type_id].present?
      search_teams = UsersSportType unless search_teams
      root_sport_type = SportType.find(params[:sport_type_id])
      sport_types_ids = root_sport_type.self_and_descendants.pluck(:id)
      search_teams = search_teams.where('sport_type_id IN(?)', sport_types_ids)
    end

    if params[:level].present?
      search_teams = UsersSportType unless search_teams
      search_teams = search_teams.where(level: params[:level])
    end
    if params[:occupation_kind].present?
      search_occupations = Occupation unless search_occupations
      search_occupations = search_occupations.where(kind: params[:occupation_kind])
    end
    if params[:occupation_name].present?
      search_occupations = Occupation unless search_occupations
      search_occupations = search_occupations.where('name LIKE ?', params[:occupation_name])
    end

    if search_teams
      users = users.where('id IN (?)', search_teams.all.map(&:user_id))
    end

    if search_occupations
      users = users.where('id IN (?)', search_occupations.all.map(&:user_id))
    end

    users
  end

  def active_for_authentication?
    super && !self.banned
  end

end
