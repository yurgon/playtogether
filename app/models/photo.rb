class Photo < ActiveRecord::Base
  attr_accessible :date, :description, :photo, :photoalbum_id, :user_id

  has_many :comments, as: :commentable, order: 'date', dependent: :destroy

  belongs_to :user
  belongs_to :photoalbum, touch: true

  has_many :likes, as: :likeable

  mount_uploader :photo, PhotoUploader

  def next_photo
    Photo.where(photoalbum_id: self.photoalbum_id).where('id < ?', self.id).order('id DESC').limit(1).first
  end

  def prev_photo
    Photo.where(photoalbum_id: self.photoalbum_id).where('id > ?', self.id).order('id').limit(1).first
  end


  def to_jq_upload
    {
        "name" => self.photo.filename,
        "size" => self.photo.size,
        "url" => self.photo.url,
        #"delete_url" => upload_path(self),
        #"delete_type" => "DELETE"
    }
  end

end
