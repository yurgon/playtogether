class CommunityRole < ActiveRecord::Base
  attr_accessible :community_id, :role, :user_id, :role_description, :number, :status

  belongs_to :user
  belongs_to :community

  COMMUNITY_ROLES = {
      'Администратор' => 'admin',
  }

  TEAM_ROLES = {
      'Участник команды' => 'member',
      'Администратор' => 'admin',
  }

  scope :admin, where(role: 'admin')
  scope :team_member, where(role: 'team_member')
  scope :member, where(role: 'member')

  validates :role, :presence => true, :allow_blank => false, :inclusion => {:in => TEAM_ROLES.values}



end
