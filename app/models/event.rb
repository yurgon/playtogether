class Event < ActiveRecord::Base
  attr_accessible :date, :description, :kind, :name, :team1_id, :team2_id, :user_id, :spot_id, :place_id, :invite_team1_id, :invite_team2_id, :result
  attr_accessor :invite_team1_id, :invite_team2_id

  after_save :processing_invites

  has_many :invites
  has_many :accepted_event_members
  has_many :photoalbums, :as => :photoalbumable, order: 'created_at DESC'
  has_many :videoalbums, :as => :videoalbumable, order: 'created_at DESC'

  KINDS = {'Встреча' => 0, 'Тренировка' => 1, 'Игра' => 2}

  belongs_to :place
  belongs_to :user

  validates_presence_of :name

  has_many :comments, as: :commentable, order: 'date DESC', dependent: :destroy
  has_many :favorites, as: :favoritable, dependent: :destroy
  has_many :event_subscribes

  def fullname
    self.name
  end

  def accepted_teams
    result = []
    result << self.team1_id if self.team1_id
    result << self.team2_id if self.team2_id
    result
  end

  def accepted_members(team_id)
    user_ids = self.accepted_event_members.where(:team_id => team_id).pluck(:user_id)
    User.where :id => user_ids
  end

  def user_can_accept_as_team_member(user)
    user.community_roles.where(role:'member', community_id:self.accepted_teams).present?
  end

  def has_accepted_team_member(user)
    user.accepted_event_members.where(:event_id => self.id).present?
  end

  include ActionView::Helpers

  def full_description
    truncate(self.description, length: 300)
  end

  validate :validate_teams

  def self.search(params)
    events = self
    if params[:q].present?
      events = events.where('description LIKE ? OR name LIKE ?', "%#{params[:q]}%", "%#{params[:q]}%")
    end
    if params[:name].present?
      events = events.where('name LIKE ?', "%#{params[:name]}%")
    end

    if params[:place].present?
      place = Place.find(params[:place])
      places_ids = place.self_and_descendants.pluck(:id)
      events = events.where('place_id IN(?)', places_ids)
    end
    if params[:past].present?
      events = events.where('date < ?', Time.now)
    else
      events = events.where('date > ?', Time.now)
    end
    if params[:start_date].present?
      date = Date.parse(params[:start_date])
      events = events.where(date: date.beginning_of_day..date.end_of_day)
    end
    if params[:finish_date].present?
      date = Date.parse(params[:finish_date])
      events = events.where(date: date.beginning_of_day..date.end_of_day)
    end

    events
  end


  protected

  def validate_teams
    result = false
    if self.id.present?
      result = true
    else
      if !self.kind.nil? && self.kind == 1
        result = self.invite_team1_id && self.invite_team1_id != ''
      elsif self.kind == 2
        result = self.invite_team1_id && self.invite_team2_id && self.invite_team1_id != '' && self.invite_team2_id != ''
      else
        result = true
      end
    end
    unless result
      errors.add(:invite_team1_id, 'должно быть заполнено') if !self.invite_team1_id || self.invite_team1_id == ''
      errors.add(:invite_team2_id, 'должно быть заполнено') if !self.invite_team2_id || self.invite_team2_id == ''
    end
  end

  def must_have_one_team?
    !self.kind.nil? && self.kind > 0
  end

  def must_have_two_team?
    !self.kind.nil? && self.kind > 1
  end

  def processing_invites
    if self.invite_team1_id || self.invite_team2_id
      self.invites.destroy_all
      if self.kind == 1
        check_and_send_invite self.invite_team1_id, 'team1_id'
      elsif self.kind == 2
        check_and_send_invite self.invite_team1_id, 'team1_id'
        check_and_send_invite self.invite_team2_id, 'team2_id'
      end
    end

  end

  def check_and_send_invite(invite_team_id, cur_team_id_column)
    if !self.send(cur_team_id_column) || self.send(cur_team_id_column) != invite_team_id
      self.send(cur_team_id_column + '=', nil)
      team = Team.find(invite_team_id)
      self.invites.create community_id: team.id, admin_id: team.users_with_roles.where('community_roles.role = "admin"').first.id if invite_team_id
    end
  end

end
