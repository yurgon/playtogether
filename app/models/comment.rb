class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :commentable, polymorphic: true
  belongs_to :retweetable, polymorphic: true

  attr_accessible :commentable_id, :commentable_type, :content, :date, :user_id, :retweetable_id, :retweetable_type
  validates_presence_of :content

  has_many :comments, as: :commentable, order: 'date', dependent: :destroy
  has_many :likes, as: :likeable


  def wall?
    ['User','Community','Ad','Spot','SportEvent'].include? self.commentable_type
  end

end
