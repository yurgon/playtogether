class MembersRequest < ActiveRecord::Base
  attr_accessible :user_id, :community_id
  belongs_to :community
  belongs_to :user
end
