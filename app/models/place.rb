class Place < ActiveRecord::Base
  attr_accessible :depth, :lft, :name, :parent_id, :rgt

  acts_as_nested_set

  scope :root_scope, where("parent_id IS NULL")
  scope :two_level_scope, where("depth < 2")
  scope :with_depth_below, lambda { |level| where(self.arel_table[:depth].lt(level)) }
  scope :ordering, order('name ASC')

  def fullname
    self.name
  end

  def to_parent(type)
     places = []
     self.self_and_ancestors.each do |place|
       places <<  place[type]
     end
     places
  end

  def to_child
    counter = true
    places = []
    places << self.id
    a = self
    Place.all.each{|p|
      if p.to_parent("id").include?(a.id)
        count_a = false
        s = p.to_parent("id").map{|u|
          if count_a == false
            if u == a.id
              count_a = true
            end
          end
          if count_a == true
            u
          end
        }
        s.delete(nil)
        places = places | s
      end
    }
    places.flatten
  end


end
