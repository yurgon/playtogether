class Ability
  ADMINS = ['office@web-creator.ru', 'petr@web-creator.ru', 'artem.savichev@mail.ru', 'gav09@yandex.ru']
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)
    if user.email == 'gav09@yandex.ru' || user.email == "mymailforxperia@gmail.com" || user.email.downcase == "amcorp@yandex.ru" 
       can :manage, User
       can :manage, KeyValue
       can :manage, Photoalbum
       can :manage, Videoalbum
       can :manage, Event
       can :manage, SportEvent
       can :manage, Community
       can :manage, Spot
       can :manage, Ad
       can :manage, Place
       can :manage, SportType

    end
      can :update, User, :id => user.id


      can :read, KeyValue

      can :read, User
      can :friends, User
      can :add_to_friends, User
      can :messages, User
      can :messages, User

      can :read, Message, :receiver_id => user.id, receiver_status: [0,1]
      can :read, Message, :sender_id => user.id, sender_status: [0,1]
      can :destroy, Message, :receiver_id => user.id, receiver_status: [0,1]
      can :destroy, Message, :sender_id => user.id, sender_status: [0,1]


      can :read, Photoalbum
      can :create, Photoalbum
      can :update, Photoalbum
      can :destroy, Photoalbum
      can :create, Photoalbum, :photoalbumable_id => user.community_roles.admin.pluck(:community_id)
      can :update, Photoalbum, :photoalbumable_id => user.community_roles.admin.pluck(:community_id)
      can :destroy, Photoalbum, :photoalbumable_id => user.community_roles.admin.pluck(:community_id)

      can :read, Videoalbum
      can :create, Videoalbum
      can :update, Videoalbum
      can :destroy, Videoalbum
      can :create, Videoalbum, :videoalbumable_id => user.community_roles.admin.pluck(:community_id)
      can :update, Videoalbum, :videoalbumable_id => user.community_roles.admin.pluck(:community_id)
      can :destroy, Videoalbum, :videoalbumable_id => user.community_roles.admin.pluck(:community_id)


      can :read, Event
      can :create, Event, :user_id => user.id
      can :update, Event, :user_id => user.id
      can :destroy, Event, :user_id => user.id

      can :read, Community
      can :create, Community
      can :memberships, Community
      can :membership_cancel, Community
      can :membership_invite, Community
      can :membership_request, Community
      can :update, Community, id: user.communities_with_roles.where('community_roles.role = "admin"').pluck(:id)

      can :read, Spot
      can :create, Spot
      can :update, Spot, :user_id => user.id
      can :destroy, Spot, :user_id => user.id
      can :read, Ad
      can :create, Ad
      can :update, Ad, :user_id => user.id
      can :destroy, Ad, :user_id => user.id

      can :destroy, Comment, :user_id => user.id

    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
