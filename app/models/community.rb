class Community < ActiveRecord::Base

  attr_accessible :about, :name, :avatar, :type, :place_id, :sport_type_id, :team_player_searches_attributes
  validates_presence_of :name, :type

  COMMUNITY_TYPES = {
      'Команда' => 'Team',
      'Группа' => 'Infogroup'
  }

  validates :type, :presence => true, :allow_blank => false, :inclusion => {:in => COMMUNITY_TYPES.values}

  mount_uploader :avatar, AvatarUploader

  has_many :photoalbums, :as => :photoalbumable, order: 'created_at DESC'
  has_many :videoalbums, :as => :videoalbumable, order: 'created_at DESC'
  has_many :members_requests

  has_many :comments, as: :commentable, order: 'date DESC', dependent: :destroy

  has_many :team_player_searches, :autosave => true
  accepts_nested_attributes_for :team_player_searches, :allow_destroy => true

  has_many :community_roles, dependent: :destroy
  accepts_nested_attributes_for :community_roles, :allow_destroy => true
  attr_accessible :community_roles_attributes


  has_many :users_with_roles, through: :community_roles, :source => :user

  has_many :memberships, dependent: :destroy
  has_many :users, through: :memberships

  def members
    User.where :id => self.community_roles.where('community_roles.role = "member"').pluck(:user_id)
  end

  belongs_to :place
  belongs_to :sport_type

  def is_admin?(user_id)
    self.get_user_roles(user_id).where('community_roles.role = ?', 'admin').any?
  end

  def is_member?(user_id)
    self.get_user_roles(user_id).where('community_roles.role = ?', 'member').any?
  end

  def get_user_roles(user_id)
    self.community_roles.where('community_roles.user_id = ?', user_id)
  end


end
