class Video < ActiveRecord::Base

  attr_accessible :date, :description, :provider, :user_id, :video, :videoalbum_id
  validates_presence_of :video

  has_many :comments, as: :commentable, order: 'date', dependent: :destroy
  has_many :likes, as: :likeable

  belongs_to :user
  belongs_to :videoalbum, touch: true

  def thumb
    if provider == 'YouTube'
      'http://img.youtube.com/vi/'+video+'/hqdefault.jpg'
    else
      '/img/thumb_no_photo.png'
    end
  end

  before_validation :detect_video_id

  def detect_video_id
    if self.video[/youtu\.be\/([^\?]*)/]
      self.video = $1
      self.provider = 'YouTube'
    else
      # Regex from # http://stackoverflow.com/questions/3452546/javascript-regex-how-to-get-youtube-video-id-from-url/4811367#4811367
      self.video[/^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*/]
      self.video = $5
      self.provider = 'YouTube' unless self.video.nil?
    end

  end

  def video_frame
    '<iframe width="640" height="360" src="//www.youtube.com/embed/'+video+'?autoplay=1" frameborder="0" allowfullscreen></iframe>'
  end

  def video_frame_without_autoplay
    '<iframe width="640" height="360" src="//www.youtube.com/embed/'+video+'?frameborder="0" allowfullscreen></iframe>'
  end


  def next_video
    Video.where(videoalbum_id: self.videoalbum_id).where('id < ?', self.id).order('id DESC').limit(1).first
  end

  def prev_video
    Video.where(videoalbum_id: self.videoalbum_id).where('id > ?', self.id).order('id').limit(1).first
  end

end
