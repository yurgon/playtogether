class TeamPlayerSearch < ActiveRecord::Base
  belongs_to :community
  belongs_to :sport_type

  attr_accessible :level, :sport_type_id
  validates_presence_of :sport_type_id

end
