class Ad < ActiveRecord::Base
  attr_accessible :description, :kind, :name, :price, :place_id, :user_id

  validates_presence_of :name

  belongs_to :place
  belongs_to :user

  has_many :comments, as: :commentable, order: 'date DESC', dependent: :destroy

  has_many :photoalbums, :as => :photoalbumable, order: 'created_at DESC'
  has_many :videoalbums, :as => :videoalbumable, order: 'created_at DESC'

  KINDS = {'Куплю' => 0, 'Продам' => 1, 'Обменяю' => 2, 'Отдам' => 3}

  has_many :favorites, as: :favoritable, dependent: :destroy

  def fullname
    Ad::KINDS.invert[self.kind]+': '+self.name;
  end


  include ActionView::Helpers

  def full_description
    truncate(self.description, length: 300)
  end



  def self.search(params)
    teams = self
    if params[:q].present?
      teams = teams.where('description LIKE ? OR name LIKE ?', "%#{params[:q]}%", "%#{params[:q]}%")
    end
    if params[:name].present?
      teams = teams.where('name LIKE ?', "%#{params[:name]}%")
    end

    teams
  end


end
