class Infogroup < Community

  def self.model_name
    Community.model_name
  end


  def self.search(params)
    teams = self
    if params[:q].present?
      teams = teams.where('about LIKE ? OR name LIKE ?', "%#{params[:q]}%", "%#{params[:q]}%")
    end
    if params[:name].present?
      teams = teams.where('name LIKE ?', "%#{params[:name]}%")
    end

    if params[:sport_type].present?
      teams = teams.where('sport_type_id = ?', params[:sport_type])
    end

    if params[:place].present?
      place = Place.find(params[:place])
      places_ids = place.self_and_descendants.pluck(:id)
      teams = teams.where('place_id IN(?)', places_ids)
    end

    teams
  end


end