class Membership < ActiveRecord::Base
  attr_accessible :community_id, :community_status, :user_id, :user_status

  belongs_to :user
  belongs_to :community


  after_initialize :default_values

  def default_values
    self.community_status ||= 0
    self.user_status ||= 0
  end



end
