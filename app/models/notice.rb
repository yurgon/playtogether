class Notice < ActiveRecord::Base
  attr_accessible :text, :theme, :type, :user_id


  NOTICE_TYPES = {
      'E-mail' => 'NoticeEmail',
      'SMS-сообщение' => 'NoticeSMS',
      'Сообщение на сайте' => 'NoticeInternal',
  }

  validates :type, :presence => true, :allow_blank => false, :inclusion => {:in => NOTICE_TYPES.values}



end
