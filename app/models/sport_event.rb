class SportEvent < ActiveRecord::Base
  attr_accessible :announce, :content, :header, :date, :image


  has_many :comments, as: :commentable, order: 'date', dependent: :destroy

  has_many :favorites, as: :favoritable, dependent: :destroy

  has_many :photoalbums, :as => :photoalbumable, order: 'created_at DESC'
  has_many :videoalbums, :as => :videoalbumable, order: 'created_at DESC'

  mount_uploader :image, AvatarUploader

  def fullname
    self.header
  end

  def full_description
    self.announce
  end


end
