class Favorite < ActiveRecord::Base
  attr_accessible :favoritable_id, :favoritable_type, :user_id

  belongs_to :favoritable, polymorphic: true

end
