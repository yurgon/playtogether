class Message < ActiveRecord::Base
  attr_accessible :content, :receiver_id, :sender_id, :receiver_status, :sender_status

  belongs_to :receiver, class_name: 'User', foreign_key: 'receiver_id'
  belongs_to :sender, class_name: 'User', foreign_key: 'sender_id'



  after_initialize :default_values

  def default_values
    self.sender_status ||= 1
    self.receiver_status ||= 0
  end



end
