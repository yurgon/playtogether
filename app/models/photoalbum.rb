class Photoalbum < ActiveRecord::Base
  attr_accessible :community_id, :name, :user_id
  has_many :photos, order: 'id DESC'
  belongs_to :photoalbumable, polymorphic: true

end
