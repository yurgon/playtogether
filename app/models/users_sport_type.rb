class UsersSportType < ActiveRecord::Base
  attr_accessible :level, :search_team, :sport_type_id, :user_id

  belongs_to :user
  belongs_to :sport_type

  LEVELS = { 'Начинающий'=>10, 'Любитель'=>20, 'Полупрофессионал'=>30, 'Профессионал'=>30 }
end
