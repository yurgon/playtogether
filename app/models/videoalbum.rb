class Videoalbum < ActiveRecord::Base
  attr_accessible :group_id, :name, :user_id
  has_many :videos, order: 'created_at DESC'
  belongs_to :videoalbumable, polymorphic: true

  
end
