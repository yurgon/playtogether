class Image < ActiveRecord::Base

  attr_accessible :file, :name, :obj_id, :obj_type, :file_cache
  belongs_to :imageable, polymorphic: true
  acts_as_list scope: [:imageable_id, :imageable_type]

  mount_uploader :file, ImageUploader
  validates :file, :presence => { message: 'Файл должен быть выбран'}

  # HASH LINKS
  # 20 первых символов => ID
  # 10 последних символов => версия
  def link(version=nil)

    hash = Digest::MD5.hexdigest('self.updated_at')
    id = self.id.to_s
    name = hash[0, 19-id.length] + 'abcdefabcd'[id[0].to_i]+id+hash[29, 1]
    file_name = File.basename(self.file.path)

    if version.nil?
      name += 'bdecfbdecf'[id[0].to_i]+hash[19, 9]
    else
      name += version.gsub('x','a') + 'abdeabdeab'[id[0].to_i] + hash[19, 10-version.length]
    end

    '/images/'+name+'/'+file_name

  end

  # return path (or nil on error)
  def self.get_path(hash)

    return nil if hash.length != 32

    id = hash[0, 20].scan(/[0-9]{1,20}$/).first

    return nil if id.nil?

    image = Image.find(id)

    # get thumb parameters, format: [width,height,mode]
    version = hash[21, 10].scan(/^([0-9]{0,4})[a]?([0-9]{0,4})([cf]?)/).first

    # crop & fill works if set width & height
    return nil if version[0].blank? && version[1].blank? && !version[2].blank?


    if version[0].blank? && version[1].blank?

      # return original if width & height is undefined
      image.file.path


    else

      # return thumbnail if width & height is defined

      filename = ''
      filename+= version[2] unless version[2].blank?
      filename+= version[0] unless version[0].blank?
      filename+= 'x'+version[1] unless version[1].blank?
      filename+= '_'+hash[20, 2]+hash[0, 3]
      filename+= '_'+File.basename(image.file.path)

      path = './tmp/thumbs/'+id

      unless File.exists?(path+'/'+filename)

        Dir.mkdir('./tmp/thumbs') unless File.exists?('./tmp/thumbs')
        Dir.mkdir(path) unless File.exists?(path)

        img = MiniMagick::Image.open(image.file.path)

        if version[2].blank?

          command = ''
          command+= version[0] unless version[0].blank?
          command+= 'x'+version[1] unless version[1].blank?
          img.resize(command+'>')

        else

          # приведение к четному
          resize_w = version[0]
          resize_h = version[1]
          resize_w = img[:width] - 1 if img[:width] < version[0].to_i && img[:width]%2==1
          resize_h = img[:height] - 1 if img[:height] < version[1].to_i && img[:height]%2==1
          command = resize_w.to_s+'x'+resize_h.to_s

          if version[2] == 'c'

            if (version[0].to_f / img[:width].to_f) > (version[1].to_f / img[:height].to_f)
              crop_resize = version[0]
            else
              crop_resize = 'x'+version[1]
            end

            img.resize(crop_resize+'>')

            horizontal_offset = (version[0].to_i-img[:width]) / 2
            vertical_offset = (version[1].to_i-img[:height]) / 2
            horizontal_offset = 0 if horizontal_offset < 0
            vertical_offset = 0 if vertical_offset < 0

            if horizontal_offset > 0 || vertical_offset > 0
              img.combine_options do |i|
                i.bordercolor('#ffffff')
                i.border(horizontal_offset.to_s+'x'+vertical_offset.to_s)
              end
            end

            horizontal_offset = (img[:width]-version[0].to_i) / 2
            vertical_offset = (img[:height]-version[1].to_i) / 2
            img.crop(command+'+'+horizontal_offset.to_s+'+'+vertical_offset.to_s)

          elsif version[2] == 'f'

            img.resize(command+'>')
            horizontal_offset = (version[0].to_i-img[:width]) / 2
            vertical_offset = (version[1].to_i-img[:height]) / 2
            if horizontal_offset > 0 || vertical_offset > 0
              img.combine_options do |i|
                i.border(horizontal_offset.to_s+'x'+vertical_offset.to_s)
                i.bordercolor('#ffffff')
              end
            end

          end
        end

        img.quality('100')
        img.write path+'/'+filename

      end

      path+'/'+filename

    end

  end

end
