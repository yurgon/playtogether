class SportType < ActiveRecord::Base
  attr_accessible :depth, :lft, :name, :parent_id, :rgt

  acts_as_nested_set
  scope :with_depth_below, lambda { |level| where(self.arel_table[:depth].lt(level)).order(:lft) }
  scope :ordering, order('name ASC')

  def fullname
    fullname = self.name
    if self.level > 1
      fullname = self.parent.name + ', ' + fullname
    end

    fullname

  end
end
