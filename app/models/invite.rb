class Invite < ActiveRecord::Base
  attr_accessible :community_id, :event_id, :admin_id
  belongs_to :event
  belongs_to :community
end
