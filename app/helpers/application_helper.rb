module ApplicationHelper

  def timeago(time)
    timestr = ''

    time_dist = (Time.now - time).round
    if time_dist < 60
      timestr = 'только что'
    elsif time_dist < 59*60
      time_dist = (time_dist/60).round
      timestr = time_dist.to_s + ' ' + Russian.p(time_dist, 'минуту', 'минуты', 'минут') + ' назад'
    elsif Date.today == time.to_date
      timestr = 'сегодня в ' + Russian::strftime(time, '%H:%M')
    elsif (Date.today-1.day) == time.to_date
      timestr = 'вчера в ' + Russian::strftime(time, '%H:%M')
    elsif (Date.today-2.day) == time.to_date
      timestr = 'позавчера в ' + Russian::strftime(time, '%H:%M')
    else
      timestr = Russian::strftime(time, '%d %B %Y в %H:%M')
    end

    timestr
  end



  def deadline(time)
    timestr = ''

    time_dist = (time-Time.now).round
    if time_dist < 0
      timestr =''
    elsif time_dist < 60
      timestr = 'до мероприятия несколько секунд'
    elsif time_dist < 59*60
      time_dist = (time_dist/60).round
      timestr = 'до мероприятия '+time_dist.to_s + ' ' + Russian.p(time_dist, 'минута', 'минуты', 'минут') + ''
    elsif time_dist < 60*60*12
      time_dist = (time_dist/60/60).round
      timestr = 'до мероприятия '+time_dist.to_s + ' ' + Russian.p(time_dist, 'час', 'часа', 'часов') + ''
    else
      time_dist = (time.to_date - Date.today).round
      timestr = 'до мероприятия '+time_dist.to_s + ' ' + Russian.p(time_dist, 'день', 'дня', 'дней') + ''
    end

    timestr
  end

  def online_status(user)

    status = ''
    unless user.online_time.nil?

      status = timeago(user.online_time)
      if user.sex === false
        status = 'Заходила ' + status
      else
        status = 'Заходил ' + status
      end

    end
    status
  end

  def is_english
    params['language'] == 'eng' || current_user && current_user.language == 'eng'
  end

end
