class Emailer < ActionMailer::Base
  default from: "no_reply@playtoget.com"

  def invite_to_friend(user,email,user_new)
    @user = user
    @email = email
    @user_new = user_new
    mail(to: @email, subject: "Ваш друг приглавил Вас")
  end

  def invite_to_friend_in(user,email)
    @user = user
    @email = email
    mail(to: @email, subject: "Вам отправлен запрос на дружбу")
  end
  
  def invite_to_group(user,email,group)
   @user = user
   @email = email
   @group = group
   mail(to: @email,subject: "В вашу группу вступил #{@user.name}")
  end
 
  def input_messages(user,email,message)
    @message = message
    @user = user
    @email = email
    mail(to: @email, subject: "Вам отправлено сообщение от #{@user.name}")
  end
  
  def posting_updates(email,message)
    @email = email
    @message = message
    mail(to: @email, subject: "Обновления на сайте PlayToGet.com")
  end

end
