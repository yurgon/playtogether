class Notify < ActionMailer::Base
  default from: "no_reply@playtoget.com"

  def members_email(user, text)
    @user = user
    @text = text
    mail(to: @user.email, subject: 'Оповещение PlayTogether')
  end
end
