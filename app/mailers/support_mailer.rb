class SupportMailer < ActionMailer::Base
  default from: 'no_reply@playtoget.com'

  def support_email(email, text, from_user)
    @user_email = from_user
    @text = text
    mail(to: email, subject: 'Обратная связь PlayTogether')
  end
end
