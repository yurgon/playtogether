# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  $("#place_id").change ->
    id = $(this).val()
    $(this).attr("data-place-id",id)
    $.get "/search_comm?type=Infogroup&place_id=" + id + "&sport_type_id=" + $("#sport_type").attr("data-sport-id"), (data)->
      $(".all_comands").empty()
      $(".all_comands").append(data)
    return false
  return true
$(document).ready ->
  $("#sport_type").change ->
    id = $(this).val()
    $(this).attr("data-sport-id",id)
    $.get "/search_comm?type=Infogroup&place_id=" + $("#place_id").attr("data-place-id") + "&sport_type_id=" + id, (data)->
      $(".all_comands").empty()
      $(".all_comands").append(data)
    return false
  return true
$(document).ready ->
  $("#start_date_event").change ->
    id = $(this).val()
    $(this).attr("data-date-event",id)
    $.get "/search_event?place_id_event=" + $("#place_id_event").attr("data-place-id") + "&date=" + id, (data)->
      $(".event_all").empty()
      $(".event_all").append(data)
    return false
  return true
$(document).ready ->
  $("#place_id_event").change ->
    id = $(this).val()
    $(this).attr("data-place-id",id)
    $.get "/search_event?place_id_event=" + id + "&date=" + $("#start_date_event").attr("data-date-event"), (data)->
      $(".event_all").empty()
      $(".event_all").append(data)
    return false
  return true
