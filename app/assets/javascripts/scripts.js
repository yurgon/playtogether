$(document).ready(function(){
  $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
  $.datepicker.regional['ru'] = {
    closeText: 'Закрыть',
    prevText: '&#x3c;Пред',
    nextText: 'След&#x3e;',
    currentText: 'Сегодня',
    monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
    'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
    monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
    'Июл','Авг','Сен','Окт','Ноя','Дек'],
    dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
    dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
    dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);

    $('input.datepicker').datepicker({
    showOn: 'both',
    buttonImageOnly: true,
    buttonImage: '/images/026.png'
    });
  $(".modal_link").click(function() {
    $("#modal").removeClass("hide");
  });

  $('#comment_content').bind('keydown', 'return', function() {
    $('#new_comment').submit();
    return false;
    });
    $('#comment_content').bind('keydown', 'Ctrl+return', function() {
    breakText_rangyinputs();
    });
    var top_menu = $('.header-right').children('ul').children('li').children('a[data-aside]');
     top_menu.attr('data-open', 'false');
     top_menu.click(function(){
          top_menu.each(function(i,elem){
               $(elem).removeClass('activ');
          });
          $(this).addClass('activ');
     });

     // top_menu.click(function(){
     //      if ($(this).attr('data-open') === 'false') {
     //           $('aside, main, footer, .postText, .foot').removeAttr('style');
     //           $('main').css('overflow', 'hidden');
     //           top_menu.attr('data-open', 'false');
     //           $(this).attr('data-open', 'true');
     //           $('aside').show();
     //           $('main').width($('main').width() - 220);
     //           $('footer').width($('footer').width() - 220);
     //           $('.foot').css('margin-right', '222px')
     //      } else {
     //           top_menu.attr('data-open', 'false');
     //           $(this).removeClass('activ');
     //           $('aside, main, section, footer, .foot').removeAttr('style');
     //           $('main').css('overflow', 'hidden');
     //      }
     // });

     var left_menu = $('.leftMenu').children('li').children('a');
     left_menu.click(function(e){
          left_menu.each(function(i,elem){
               $(elem).removeClass('leftActive')
          });
          $(this).addClass('leftActive');
     });


     $('[type="search"]').focus(function(){
          $('.search').addClass('searchActive').removeClass('search');
     });
     $('[type="search"]').blur(function(){
          $('.searchActive').addClass('search').removeClass('searchActive');
     });

     fullscreen();
     $(window).resize(fullscreen);

     function fullscreen() {
          var masthead = $('.videoBox');
          var windowH = $(window).height();
          var windowW = $(window).width();

          masthead.width(windowW);
          masthead.height(windowH);
     }
     mainPosition();

    $("#coverImageForm").change(function (){
         $(this).submit();
    });


    $("#user_avatar").change(function (){
        $("form.edit_user").submit();
    });
    $("#user_cover_page").change(function (){
         $("form.edit_user").submit();
    });
     $('textarea').keydown(function() {
          if (event.keyCode == 13) {
               $('#my_form').submit();

            }


     });
     $(function() {
        $( "#tabs" ).tabs();
      });
});

function mainPosition() {
     var main_height = $('.mainRegistr').height();
     var window_height = $(window).height();
     //$('.mainRegistr').css('top', (window_height / 2) - (main_height / 2) + 'px')

}

$(window).resize(function(){
     mainPosition();
     mainPosition();
     $('aside, main, section, footer').removeAttr('style');
     $('main').css('overflow', 'hidden');

     var tru = 'false';
     // var top_menu = $('.header-right').children('ul').children('li').children('a[data-aside]');
     // top_menu.each(function(i, elem){
     //     if($(elem).attr('data-open') === 'true') {
     //          tru = 'true'
     //     }

     // });

     // if(tru === 'true') {
     //      $('aside, main, footer, .postText, .foot').removeAttr('style');
     //      $('main').css('overflow', 'hidden');
     //      $('aside').show();
     //      $('main').width($('main').width() - 220);
     //      $('footer').width($('footer').width() - 220);
     //      $('.foot').css('margin-right', '222px')
     // }
});

var coverVid = function (elem, width, height) {

     // call sizeVideo on load
     document.addEventListener('DOMContentLoaded', sizeVideo);

     // debounce for resize function
     function debounce(fn, delay) {
          var timer = null;

          return function () {
               var context = this,
                   args = arguments;

               window.clearTimeout(timer);

               timer = window.setTimeout(function () {
                    fn.apply(context, args);
               }, delay);
          };
     }

     // call sizeVideo on resize
     window.onresize = function () {
          debounce(sizeVideo(), 50);
     };

     // Set necessary styles to position video "center center"
     elem.style.position = 'absolute';
     elem.style.top = '50%';
     elem.style.left = '50%';
     elem.style['-webkit-transform'] = 'translate(-50%, -50%)';
     elem.style['-ms-transform'] = 'translate(-50%, -50%)';
     elem.style.transform = 'translate(-50%, -50%)';

     // Set overflow hidden on parent element
     elem.parentNode.style.overflow = 'hidden';


     // Define the attached selector
     function sizeVideo() {

          // Get parent element height and width
          var parentHeight = elem.parentNode.offsetHeight;
          var parentWidth = elem.parentNode.offsetWidth;

          // Get native video width and height
          var nativeWidth = width;
          var nativeHeight = height;

          // Get the scale factors
          var heightScaleFactor = parentHeight / nativeHeight;
          var widthScaleFactor = parentWidth / nativeWidth;

          // Based on highest scale factor set width and height
          if (widthScaleFactor > heightScaleFactor) {
               elem.style.height = 'auto';
               elem.style.width = parentWidth+'px';
          } else {
               elem.style.height = parentHeight+'px';
               elem.style.width = 'auto';
          }

     }
};

function chooseFile() {
     $("#user_cover_page").click();
}

function choosePhotos() {
     $("#photo_photo").click();
}

if (window.jQuery) {
     jQuery.fn.extend({
          'coverVid': function () {
               coverVid(this[0], arguments[0], arguments[1]);
               return this;
          }
     });
}
