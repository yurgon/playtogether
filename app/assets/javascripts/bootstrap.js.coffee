jQuery ->
  $("a[rel~=popover], .has-popover").popover()
  $("a[rel~=tooltip], .has-tooltip").tooltip()

$ ->
  $('.chosen-select').chosen
    allow_single_deselect: true
    no_results_text: 'Ничего не найдено'
