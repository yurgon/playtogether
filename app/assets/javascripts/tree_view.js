function expand_node(node, recursive) {
    var id = node.attr('id').replace('node_', '');
    var controller = node.parents('.node_tree').attr('id');

    if ($.trim(node.children('.node_expander').html()) != '<i class="fa fa-circle"></i>') {

        node.children('.node_expander').html('<i class="fa fa-minus-circle fa-spin"></i>');
        node.children('.node_children').load('/admin/'+controller+'/' + id + '/children_nodes', function (responseText) {

            if ($.trim(responseText) == '') {
                node.children('.node_expander').html('<i class="fa fa-circle"></i>');
            } else {
                $(this).show();
                node.children('.node_expander').html('<i class="fa fa-minus-circle"></i>');
                if (typeof(recursive) === 'undefined') recursive = 0;
                if (recursive != 0) {
                    node.children('.node_children').children('.node').each(function () {
                        expand_node($(this), recursive - 1);
                    })
                }
            }
        });

    }
}

function collapse_node(node) {
    node.children('.node_children').hide();
    if ($.trim(node.children('.node_expander').html()) != '<i class="fa fa-circle"></i>') {
        node.children('.node_expander').html('<i class="fa fa-plus-circle"></i>');
    }
}


$(document).on('click', '.node_expander', function () {
    var node = $(this).parent();
    if (node.children('.node_children').is(":visible")) {
        collapse_node(node);
    } else {
        expand_node(node);
    }

});

$(document).on('click', '.expand_all', function () {
    $('.node_tree').children('.node').each(function () {
        expand_node($(this), -1);
    });
    return false;
});
$(document).on('click', '.collapse_all', function () {
    $('.node').each(function () {
        collapse_node($(this));
    });
    return false;
});