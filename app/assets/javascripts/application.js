
//= require jquery
//= require jquery-ui/tabs
//= require jquery_ujs
//= require twitter/bootstrap
//= require prettyphoto-rails
//= require jquery.infinitescroll
//= require jquery-fileupload
//= require chosen-jquery
//= require jquery_nested_form
//= require jquery.hotkeys
//= require jquery-ui/datepicker
//= require_tree .


function infinity_scroll() {

    $("#collection").infinitescroll('destroy');
    $("#collection").data('infinitescroll', null);

    $("#collection").infinitescroll({
        navSelector: "nav.pagination",
        nextSelector: "nav.pagination a[rel=next]",
        itemSelector: "#collection div.row",
        bufferPx: 1000,
        loading: { img: '/abc/img/reloading_32.gif', msgText: "", finishedMsg: "" }
    });
}


$(document).ready(function () {
    $("a[rel^='prettyPhoto']").prettyPhoto({
        show_title: false,
        social_tools: '',
        gallery_markup: '',
        deeplinking: false
    });
    infinity_scroll();
});

$(document).ready(function () {
    $(document).on('mouseenter', '#main_video', function () {
        $(this).attr('src', '/img/video_hover.jpg');
    }).on('mouseleave', '#main_video', function () {
        $(this).attr('src', '/img/video.jpg');
    });


    $(document).on("click", '.modal_link', function (event) {
        $('#modal .modal-body').load($(this).attr('href') + '?modal=true');
        $('#modal').modal();
        $('#modal_container').show();
        $('#modal_container').css('height', $(window).height() + 'px');

        return false;
    });

    $(".modal_closer").on("click", function (event) {
        $('#modal').modal('hide');
        $('#modal_container').hide();
        return false;
    });

    $("#modal_container").on("click", function (event) {
        $('#modal').modal('hide');
        $('#modal_container').hide();
        return false;
    });
    $("#modal").click(function (e) {
        e.stopPropagation();
    });

    $("#modal").on("show",function () {
        $("body").addClass("modal-open");
    }).on("hidden", function () {
            $("body").removeClass("modal-open")
            $("#modal .modal-body").html('')
        });

    $('#community_avatar').on("change", function () {
        $('#community_avatar_alert').html('<div class="alert alert-info">Выбрано новое изображение для сообщества</div>');
    });
    $('#user_avatar').on("change", function () {
        $('#user_avatar_alert').html('<div class="alert alert-info">Выбрано новое изображение для профиля</div>');
    });
    $('#user_cover_page').on("change", function () {
        $('#user_cover_page_alert').html('<div class="alert alert-info">Выбрана новая обложка для профиля</div>');
    });


    $('label.btn input:checked').parent().addClass('btn-success');
    $(document).on('change', 'label.btn input', function () {
            if ($(this).is(':checked')) {
                $(this).parent().addClass('btn-success')
            } else {
                $(this).parent().removeClass('btn-success')
            }
        }
    );

    eventKindSettings();

    $('.custom_friend_find').hide();
    $('#custom_friend_search').on('click', function () {
        if ($('.custom_friend_find:visible').length) {
            $('.custom_friend_find').hide();
        } else {
            $('.custom_friend_find').show();
        }
    })
});

function eventKindSettings() {
    function changeTeamInputs() {
        var curVal = $('select#event_kind').val();
        if ( curVal == 0) {
            $('.event_invite_team1_id').hide();
            $('.event_invite_team2_id').hide();
        } else if (curVal == 1) {
            $('.event_invite_team1_id').show();
            $('.event_invite_team2_id').hide();
        } else if (curVal == 2) {
            $('.event_invite_team1_id').show();
            $('.event_invite_team2_id').show();
        }
    };

    changeTeamInputs();
    $(document).on('change', 'select#event_kind', function () {
        changeTeamInputs();
    })
};

$(document).ready(function () {
   $(document).on('click', '#supportModal .send-btn', function () {
       var params = {email: $('#support_email').val(), body: $('#support_body').val()};
       $.post('/support/create', params, function (data) {
           $('#support_email').val('');
           $('#support_body').val('');
           $('#supportModal').modal('hide');
       });
       return false;
   });

    function makeLike() {
        var params = {type: $(this).attr('likeable-type'), id: $(this).attr('likeable-id')};
        var self = this;
        $.get('/likes/plus_minus', params, function (data) {
            var likesCountElement = $(self).parent().find('.like-count');
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
            if (+data === 0) {
                likesCountElement.text('');
            } else {
                likesCountElement.text(data);
            }
        });
    }

    $(document).on('click', 'a.like-action', function () {
        $.proxy(makeLike, this)();
        return false;
    });

    $('#modal').on('click', 'a.like-action', function () {
        $.proxy(makeLike, this)();
        return false;
    });


    function makeRetweet() {
        var params = {type: $(this).attr('retweetable-type'), id: $(this).attr('retweetable-id')};
        var self = this;
        $.get('/retweets/create', params, function (data) {
            var likesCountElement = $(self).parent().find('.retweet-count');
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
            if (+data === 0) {
                likesCountElement.text('');
            } else {
                likesCountElement.text(data);
            }
        });
    }

    $(document).on('click', 'a.retweet-action', function () {
        $.proxy(makeRetweet, this)();
        return false;
    });

    $('#modal').on('click', 'a.retweet-action', function () {
        $.proxy(makeRetweet, this)();
        return false;
    });

    $(".js-sport-level-1").on("change", function(e) {
      e.preventDefault();
      $.ajax({
        url: "/communities/get_sports/" + $(this).val() + '.json', //I think, it could be something else
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(data) {
          var options = '';
          for (var i = 0; i < data.length; i++) {
              options += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
          }
          $('.js-sport-level-2').html(options);
        }
      });
    });

});
