class RetweetsController < ApplicationController

  def create
    if params[:type].present? and params[:id].present?
      record = current_user.comments.where(retweetable_type: params[:type], retweetable_id: params[:id]).first
      unless record
        record = current_user.comments.create(retweetable_type: params[:type], retweetable_id: params[:id], user_id:current_user.id, content: 'Ретвит')
      end
    end
    render text: Comment.where(retweetable_type: params[:type], retweetable_id: params[:id]).count
  end


end