class Users::RegistrationsController < Devise::RegistrationsController
  layout "text_pages"
  def sign_up_params
    params.require(:user).permit(:email, :password, :password_confirmation, :fname, :lname, :terms, :nickname)
  end

end
