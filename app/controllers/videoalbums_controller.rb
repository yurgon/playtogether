class VideoalbumsController < ApplicationController

  before_filter :authenticate_user!

  # GET /videoalbums
  # GET /videoalbums.json
  def index

    if !params[:user_id].blank?

      @user = User.find(params[:user_id])

      @videoalbums = @user.videoalbums.accessible_by(current_ability).order("created_at DESC")

    elsif !params[:community_id].blank?

            @community = Community.find(params[:community_id])
            @videoalbums = @community.videoalbums.accessible_by(current_ability).order("created_at DESC")
    elsif !params[:event_id].blank?

      @event = Event.find(params[:event_id])
      @videoalbums = @event.videoalbums.order("created_at DESC")
    elsif !params[:spot_id].blank?

      @spot = Spot.find(params[:spot_id])
      @videoalbums = @spot.videoalbums.order("created_at DESC")
    elsif !params[:sport_event_id].blank?

      @sport_event = SportEvent.find(params[:sport_event_id])
      @videoalbums = @sport_event.videoalbums.order("created_at DESC")
    elsif !params[:ad_id].blank?

      @ad = Ad.find(params[:ad_id])
      @videoalbums = @ad.videoalbums.order("created_at DESC")

    end

    unless params[:group_id].blank?

    end

    render layout: "profile"
  end

  # GET /videoalbums/1
  # GET /videoalbums/1.json
  def show
    @videoalbum = Videoalbum.find(params[:id])

    unless params[:user_id].blank?
      @user = User.find(params[:user_id])
    end

    unless params[:community_id].blank?
      @community = Community.find(params[:community_id])
    end

    unless params[:event_id].blank?
      @event = Event.find(params[:event_id])
    end

    unless params[:spot_id].blank?
      @spot = Spot.find(params[:spot_id])
    end

    unless params[:sport_event_id].blank?
      @sport_event = SportEvent.find(params[:sport_event_id])
    end

    unless params[:ad_id].blank?
      @ad = Ad.find(params[:ad_id])
    end

    render layout: "profile"
  end

  def upload
    @videoalbum = Videoalbum.find(params[:videoalbum_id])

    unless params[:user_id].blank?
      @user = User.find(params[:user_id])
    end

    unless params[:community_id].blank?
      @community = Community.find(params[:community_id])
    end

    unless params[:event_id].blank?
      @event = Event.find(params[:event_id])
    end

    unless params[:spot_id].blank?
      @spot = Spot.find(params[:spot_id])
    end

    unless params[:sport_event_id].blank?
      @sport_event = SportEvent.find(params[:sport_event_id])
    end

    unless params[:ad_id].blank?
      @ad = Ad.find(params[:ad_id])
    end

    respond_to do |format|
      format.html { render layout: "profile"}
    end
  end


  # GET /videoalbums/new
  # GET /videoalbums/new.json
  def new
    @videoalbum = Videoalbum.new

    unless params[:user_id].blank?

      @user = User.find(params[:user_id])

    end

    unless params[:community_id].blank?
      @community = Community.find(params[:community_id])
    end

    unless params[:event_id].blank?
      @event = Event.find(params[:event_id])
    end

    unless params[:spot_id].blank?
      @spot = Spot.find(params[:spot_id])
    end

    unless params[:sport_event_id].blank?
      @sport_event = SportEvent.find(params[:sport_event_id])
    end

    unless params[:ad_id].blank?
      @ad = Ad.find(params[:ad_id])
    end

    render layout: "profile"
  end

  # GET /videoalbums/1/edit
  def edit

    unless params[:user_id].blank?

      @user = User.find(params[:user_id])

    end

    unless params[:community_id].blank?
      @community = Community.find(params[:community_id])
    end

    unless params[:event_id].blank?
      @event = Event.find(params[:event_id])
    end

    unless params[:spot_id].blank?
      @spot = Spot.find(params[:spot_id])
    end

    unless params[:sport_event_id].blank?
      @sport_event = SportEvent.find(params[:sport_event_id])
    end

    unless params[:ad_id].blank?
      @ad = Ad.find(params[:ad_id])
    end
    @videoalbum = Videoalbum.find(params[:id])
    render layout: "profile"
  end


  # POST /videoalbums
  # POST /videoalbums.json
  def create


    @videoalbum = Videoalbum.new(videoalbum_params)

    unless params[:user_id].blank?

      @user = User.find(params[:user_id])
      @videoalbum.videoalbumable = @user
      @videoalbum.save

      redirect_to '/users/'+@user.id.to_s+'/videoalbums'

    end


    unless params[:community_id].blank?
      @community = Community.find(params[:community_id])
      @videoalbum.videoalbumable = @community
      @videoalbum.save

      redirect_to '/communities/'+@community.id.to_s+'/videoalbums'
    end

    unless params[:event_id].blank?
      @event = Event.find(params[:event_id])
      @videoalbum.videoalbumable = @event
      @videoalbum.save

      redirect_to '/events/' + @event.id.to_s+'/videoalbums'
    end

    unless params[:spot_id].blank?
      @spot = Spot.find(params[:spot_id])
      @videoalbum.videoalbumable = @spot
      @videoalbum.save

      redirect_to '/spots/' + @spot.id.to_s+'/videoalbums'
    end

    unless params[:sport_event_id].blank?
      @sport_event = SportEvent.find(params[:sport_event_id])
      @videoalbum.videoalbumable = @sport_event
      @videoalbum.save

      redirect_to '/sport_events/' + @sport_event.id.to_s+'/videoalbums'
    end

    unless params[:ad_id].blank?
      @ad = Ad.find(params[:ad_id])
      @videoalbum.videoalbumable = @ad
      @videoalbum.save

      redirect_to '/ads/' + @ad.id.to_s+'/videoalbums'
    end


  end

  def update

    @videoalbum = Videoalbum.find(params[:id])
    videoalbumable = @videoalbum.videoalbumable
    @videoalbum.name = params[:videoalbum][:name]
    @videoalbum.save


    redirect_to videoalbumable

  end


  # DELETE /videoalbums/1
  # DELETE /videoalbums/1.json
  def destroy
    @videoalbum = Videoalbum.find(params[:id])
    videoalbumable = @videoalbum.videoalbumable
    @videoalbum.destroy
    redirect_to videoalbumable
  end

  private

  def videoalbum_params
    allowed = [:name]

    params.require(:videoalbum).permit(*allowed)
  end

end
