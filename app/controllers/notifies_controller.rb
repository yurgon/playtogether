class NotifiesController < ApplicationController
  def create
    @community = Community.find params['community_id']
    @users = User.where :id => params['notifies']['members']
    @text = params['notifies']['text']
    @users.each do |user|
      # Notify.members_email(user, @text).deliver
    end
    redirect_to(notify_new_path(@community.id), notice: 'Оповещения отправлены.')
  end

  def new
    @community = Community.find params['community_id']
    @members = @community.members
  end
end