class ImagesController < ApplicationController

  def download

    path = Image.get_path(params[:id])

    cache_expire = 60*60*24*365
    response.headers["Pragma"] = "public"
    response.headers["Cache-Control"] = "max-age=#{cache_expire}"
    response.headers["Expires"] = Time.at(Time.now.to_i + cache_expire).strftime("%D, %d %M % Y %H:%i:%s")

    if path.nil?
      raise ActionController::RoutingError.new('Not Found')
    end

    send_file path, :disposition => 'inline'


  end

end
