class KeyValuesController < ApplicationController
  # GET /key_values
  # GET /key_values.json

  before_filter :authenticate_user!

  def index

    redirect_to '/' unless can? :create, KeyValue

    @key_values = KeyValue.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @key_values }
    end
  end

  # GET /key_values/1
  # GET /key_values/1.json
  def show

    redirect_to '/' unless can? :create, KeyValue

    @key_value = KeyValue.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @key_value }
    end
  end

  # GET /key_values/new
  # GET /key_values/new.json
  def new

    redirect_to '/' unless can? :create, KeyValue

    @key_value = KeyValue.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @key_value }
    end
  end

  # GET /key_values/1/edit
  def edit
    redirect_to '/' unless can? :create, KeyValue
    @key_value = KeyValue.find(params[:id])
  end

  # POST /key_values
  # POST /key_values.json
  def create
    redirect_to '/' unless can? :create, KeyValue
    @key_value = KeyValue.new(key_value_params)

    respond_to do |format|
      if @key_value.save
        format.html { redirect_to key_values_path, notice: 'Успешно обновлено' }
        format.json { render json: @key_value, status: :created, location: @key_value }
      else
        format.html { render action: "new" }
        format.json { render json: @key_value.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /key_values/1
  # PATCH/PUT /key_values/1.json
  def update
    redirect_to '/' unless can? :create, KeyValue
    @key_value = KeyValue.find(params[:id])

    respond_to do |format|
      if @key_value.update_attributes(key_value_params)
        format.html { redirect_to key_values_path, notice: 'Успешно обновлено' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @key_value.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /key_values/1
  # DELETE /key_values/1.json
  def destroy
    redirect_to '/' unless can? :create, KeyValue
    @key_value = KeyValue.find(params[:id])
    @key_value.destroy

    respond_to do |format|
      format.html { redirect_to key_values_url }
      format.json { head :no_content }
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def key_value_params
      params.require(:key_value).permit(:key, :value)
    end
end
