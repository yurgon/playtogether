class UsersController < ApplicationController

  before_filter :authenticate_user!


  # GET /users
  # GET /users.json
  def index

    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
    @comments = Array.new
    @comments = @comments << @user.comments
    @user.friends.each do |f|
      @comments = @comments << f.friend.comments
    end
    @user.communities.each do |c|
      @comments << c.comments
    end
    @comments = @comments.flatten
    @comments = @comments.sort_by(&:created_at).reverse
    render layout: 'profile'
  end


  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    render layout: 'profile'
  end


  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    if @user.save
      flash.now[:notice] = 'Данные успешно сохранены'
      respond_to do |format|
        format.html { redirect_to edit_user_path(@user) }
      end
    else
      respond_to do |format|
        flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
        format.html { render action: 'new' }
      end
    end

  end


  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    if @user.update_attributes(user_params)
      flash.now[:notice] = 'Данные успешно сохранены'
    else
      flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
    end

    respond_to do |format|
      format.html {
        if params[:redirect_to] == "show"
          redirect_to "/users/#{current_user.id}"
        else
          render action: 'edit', layout: 'profile'
        end
      }
      format.js { render action: 'edit' }
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def friends

    @user = User.find(params[:id])

    if @user.id == current_user.id
      @owner = true
    else
      @owner = false
    end

    @friends = @user.friends_users.where('friends.status = 2')
    @subscribes = @user.friends_users.where('friends.status = 1')
    if @owner
      @requests = @user.friends_users.where('friends.status = 0')
      @deny = @user.friends_users.where('friends.status = -1')
    end
    render layout: 'profile'
    # respond_to do |format|
    #   format.html
    # end
  end

  def communities

    @user = User.find(params[:id])

    if @user.id == current_user.id
      @owner = true
    else
      @owner = false
    end

    @communities = @user.communities.where(type: params[:type])

    render layout: 'profile'

  end


  def add_to_friends

    @user = User.find(params[:id])

    friendship = current_user.friends.where(friend_id: @user.id).first_or_create
    reverse_friendship = @user.friends.where(friend_id: current_user.id).first_or_create
    if reverse_friendship.status == 1
      friendship.status = 2
      friendship.save
      reverse_friendship.status = 2
      reverse_friendship.save
    else
      # Emailer.invite_to_friend_in(current_user,@user.email).deliver
      friendship.status = 1
      friendship.save
    end

    respond_to do |format|
      format.html
      format.js
    end

  end

  def remove_from_friends

    @user = User.find(params[:id])

    friendship = current_user.friends.where(friend_id: @user.id).first_or_create
    reverse_friendship = @user.friends.where(friend_id: current_user.id).first_or_create

    if reverse_friendship.status == 2
      friendship.status = -1
      friendship.save
      reverse_friendship.status = 1
      reverse_friendship.save
    elsif reverse_friendship.status == 0 || reverse_friendship.status == -1
      friendship.destroy
      reverse_friendship.destroy
    else
      friendship.status = 0
      friendship.save
    end

  end

  def messages

    @user = User.find(params[:id])

    # if @user.id == current_user.id
    #   redirect_to '/messages'
    # end

    @messages = Message.accessible_by(current_ability).where('messages.sender_id='+@user.id.to_s+' OR messages.receiver_id='+@user.id.to_s).order("created_at")
    render layout: "profile"

  end

  def search_friend
    result = User
    unless params[:fname] == ''
      result = result.where 'fname like ?', "%#{params[:fname]}%"
    end
    unless params[:sname] == ''
      result = result.where 'lname like ?', "%#{params[:lname]}%"
    end
    unless params[:nick] == ''
      result = result.where 'nickname like ?', "%#{params[:nick]}%"
    end
    unless params[:text] == ''
      result = result.where 'about like ?', "%#{params[:text]}%"
    end
    if result == User
      result = User.all
    end
    if params[:sport_type] != '' && result != []
      users_ids = UsersSportType.where(:sport_type_id => params[:sport_type]).pluck(:user_id)
      users = User.where(:id => users_ids)
      result = result & users
    end
    if params[:sport_level] != '' && result != []
      users_ids = UsersSportType.where(level:params[:sport_level]).pluck(:user_id)
      raise users_ids.inspect
      users = User.where(:id => users_ids)
      result = result & users
    end
    if params[:study] != '' && result != []
      occupation_ids_institute = Occupation.where(kind:'1').where(id: params[:study]).pluck(:user_id)
      occupation_ids_school = Occupation.where(kind:'2').where(id: params[:study]).pluck(:user_id)
      users = User.where(:id => occupation_ids_school + occupation_ids_institute)
      result = result & users
    end
    if params[:work] != '' && result != []
      occupation_ids = Occupation.where(kind:'0').where(id: params[:work]).pluck(:user_id)
      users = User.where(:id => occupation_ids)
      result = result & users
    end
    friends = User.find(params[:id]).friends_users.where('friends.status = 2')
    @custom_friends = friends & result
    @found_users = (result - friends).first(15)
  end

  def invite_to_friend
    u = User.new
    u.email = params[:invite_to_friend]
    u.password = rand(32**8).to_s(32)
    u.fname = params[:invite_to_friend].split("@")[0]
    u.lname = params[:invite_to_friend].split("@")[0]
    u.save
    u.confirm!
    # Emailer.invite_to_friend(current_user,params[:invite_to_friend],u).deliver
    render nothing: true
  end

  private

  def user_params
    allowed = [:avatar, :fname, :lname, :nickname, :sex, :birthday_day, :birthday_month, :birthday_year, :email2, :phone, :about, :about_sport, :place_id,
               :permission_send_message,
               :permission_view_profile,
               :permission_view_friends,
               :permission_view_wall,
               :permission_view_photo,
               :permission_view_video,
               :permission_comment_photo,
               :permission_comment_video,
               :permission_comment_wall,
               :cover_page,
               users_sport_types_attributes: [:id, :sport_type_id, :level, :search_team, :_destroy],
               occupations_attributes: [:id, :description, :kind, :month_finish, :month_start, :name, :year_finish, :year_start, :user_id, :place_id, :_destroy]

    ]

    params.require(:user).permit(*allowed)
  end

end
