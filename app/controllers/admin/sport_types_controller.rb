class Admin::SportTypesController < ApplicationController

  def index
    authorize! :admin_index, SportType
    @sport_types = SportType.roots
  end

  def new
    authorize! :admin_new, SportType
    @sport_type = SportType.new
  end

  def create
    authorize! :admin_create, SportType
    @sport_type = SportType.new(sport_type_params)

    respond_to do |format|
      if @sport_type.save
        format.html { redirect_to admin_sport_types_path, notice: 'Добавлено.' }
        format.json { render action: 'show', status: :created, location: @sport_type }
      else
        format.html { render action: 'new' }
        format.json { render json: @sport_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    authorize! :admin_edit, SportType
    @sport_type = SportType.find(params[:id])
  end

  def update
    authorize! :admin_update, SportType
    @sport_type = SportType.find(params[:id])
    respond_to do |format|
      if @sport_type.update_attributes(sport_type_params)
        format.html { redirect_to admin_sport_types_path, notice: 'Отредактировано.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @sport_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize! :admin_destroy, SportType
    sport_type = SportType.find(params[:id])
    sport_type.destroy
    redirect_to admin_sport_types_path, notice: 'Удалено'
  end

  def children_nodes
    authorize! :admin_index, SportType
    sport_type = SportType.find(params[:id])
    if sport_type.children.any?
      render partial: 'nodes', locals: {sport_types: sport_type.children}
    else
      render :nothing => true, :status => 200, :content_type => 'text/html'
    end
  end

  def sport_type_params
    params[:sport_type].permit(:name, :parent_id)
  end
end