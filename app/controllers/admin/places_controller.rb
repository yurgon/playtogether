class Admin::PlacesController < ApplicationController

  def index
    authorize! :admin_index, Place
    @places = Place.roots
  end

  def new
    authorize! :admin_new, Place
    @place = Place.new
  end

  def create
    authorize! :admin_create, Place
    @place = Place.new(place_params)

    respond_to do |format|
      if @place.save
        format.html { redirect_to admin_places_path, notice: 'Место добавлено.' }
        format.json { render action: 'show', status: :created, location: @place }
      else
        format.html { render action: 'new' }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    authorize! :admin_edit, Place
    @place = Place.find(params[:id])
  end

  def update
    authorize! :admin_update, Place
    @place = Place.find(params[:id])
    respond_to do |format|
      if @place.update_attributes(place_params)
        format.html { redirect_to admin_places_path, notice: 'Место отредактировано.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize! :admin_destroy, Place
    place = Place.find(params[:id])
    place.destroy
    redirect_to admin_places_path, notice: 'Место удалено'
  end

  def spots_for_select
    authorize! :admin_index, Place
    place = Place.find(params[:id])
    @spots = place.descendants
    render layout: false
  end

  def children_nodes
    authorize! :admin_index, Place
    place = Place.find(params[:id])
    if place.children.any?
      render partial: 'nodes', locals: {places: place.children}
    else
      render :nothing => true, :status => 200, :content_type => 'text/html'
    end
  end

  def place_params
    params[:place].permit(:name, :parent_id)
  end
end