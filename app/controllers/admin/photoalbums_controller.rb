class Admin::PhotoalbumsController < ApplicationController

  before_filter :authenticate_user!

  # GET /photoalbums
  # GET /photoalbums.json
  def index
    @photos_sys = Photoalbum.find_by_photoalbumable_type("System").photos
    @photos_sli = Photoalbum.find_by_photoalbumable_type("Slider").photos
    @photoalbum_sys = Photoalbum.find_by_photoalbumable_type("System")
    @photoalbum_sli = Photoalbum.find_by_photoalbumable_type("Slider")
    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /photoalbums/1
  # GET /photoalbums/1.json
  def show
    @photoalbum = Photoalbum.find_by_photoalbumable_type("System")

    respond_to do |format|
      format.html
    end
  end

  def upload
    @photoalbum = Photoalbum.find(params[:photoalbum_id])

    respond_to do |format|
      format.html
    end
  end


  # GET /photoalbums/new
  # GET /photoalbums/new.json
  def new
    @photoalbum = Photoalbum.new

    respond_to do |format|
      format.html
    end
  end


  def update

    @photoalbum = Photoalbum.find(params[:id])
    photoalbumable = @photoalbum.photoalbumable
    @photoalbum.name = params[:photoalbum][:name]
    @photoalbum.save


    redirect_to photoalbumable

  end


  # DELETE /photoalbums/1
  # DELETE /photoalbums/1.json
  def destroy
    @photoalbum = Photoalbum.find(params[:id])
    photoalbumable = @photoalbum.photoalbumable
    @photoalbum.destroy
    redirect_to photoalbumable

  end

  private

  def photoalbum_params
    allowed = [:name]

    params.require(:photoalbum).permit(*allowed)
  end

end
