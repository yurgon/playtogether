class Admin::UsersController < ApplicationController

  def index
    authorize! :admin_index, User
    @users = User.all
  end

  def destroy
    authorize! :admin_destroy, User
    user = User.find(params[:id])
    if user == current_user
      redirect_to admin_users_path, notice: 'Невозможно удалить свою учетную запись'
    else
      user.destroy
      redirect_to admin_users_path, notice: 'Пользователь удален'
    end
  end

  def user_ban
    authorize! :admin_user_banned, User
    user = User.find(params[:id])
    if user == current_user
      redirect_to admin_users_path, notice: 'Невозможно заблокировать свою учетную запись'
    else
      if user.banned
        user.banned = false
        user.save
        redirect_to admin_users_path, notice: 'Пользователь разблокирован'
      else
        user.banned = true
        user.save
        redirect_to admin_users_path, notice: 'Пользователь заблокирован'
      end

    end
  end
end