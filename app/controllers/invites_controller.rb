class InvitesController < ApplicationController
  def index
    @invites = Invite.includes(:community).where(:admin_id => current_user.id)
  end

  def accept
    invite = Invite.find(params[:invite_id])
    if invite.admin_id == current_user.id
      event = Event.find(invite.event_id)
      if event.team1_id
        event.update_column(:team2_id, invite.community_id)
      else
        event.update_column(:team1_id, invite.community_id)
      end
      invite.destroy
    end
    redirect_to '/invites'
  end

  def cancel
    invite = Invite.find(params[:invite_id])
    invite.destroy if invite.admin_id == current_user.id
    redirect_to '/invites'
  end
end