class CommentsController < ApplicationController

  before_filter :authenticate_user!

  # GET /comments
  # GET /comments.json
  def index
    @comments = Comment.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    @comment = Comment.find(params[:id])

    respond_to do |format|
      format.html
    end
  end


  # GET /comments/new
  # GET /comments/new.json
  def new
    @comment = Comment.new

    respond_to do |format|
      format.html
    end
  end

  # GET /comments/1/edit
  def edit
    @comment = Comment.find(params[:id])
  end



  # POST /comments
  # POST /comments.json
  def create
    @comment = Comment.new(comment_params)

    @comment.user_id = current_user.id
    @comment.date = Time.now

    @comment.save
    @user = current_user
    if current_user.id == params[:comment][:commentable_id].to_i
      @comments = Array.new
      @comments = @comments << @user.comments
      @user.friends.each do |f|
        @comments = @comments << f.friend.comments
      end
      @user.communities.each do |c|
        @comments << c.comments
      end
      @comments = @comments.flatten
      @comments = @comments.sort_by(&:created_at).reverse
    else
      @comments = @comment.commentable.comments
    end
    session[:return_to] ||= request.referer


    respond_to do |format|
         format.html { redirect_to session.delete(:return_to) }
         format.js { render action: 'create' }
    end

  end


  # PUT /comments/1
  # PUT /comments/1.json
  def update
    @comment = Comment.find(params[:id])


    if @comment.update_attributes(comment_params)
      flash.now[:notice] = 'Данные успешно сохранены'
    else
      flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
    end

    respond_to do |format|
      format.html { render action: 'edit' }
      format.js { render action: 'edit' }
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment = Comment.find(params[:id])
    commentable = @comment.commentable
    @comment.destroy

    redirect_to commentable
  end

  private

  def comment_params
    allowed = [ :commentable_id, :commentable_type, :content ]

    params.require(:comment).permit( *allowed )
  end

end
