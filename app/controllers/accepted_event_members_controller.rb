class AcceptedEventMembersController < ApplicationController
  def create
    event = Event.find(params[:event_id])
    teams_ids = event.accepted_teams
    team_id = CommunityRole.where(:community_id => teams_ids, :user_id => current_user.id, role:'member').first.community_id
    AcceptedEventMember.create :user_id => current_user.id, :team_id => team_id, :event_id => event.id
    redirect_to event
  end

  def destroy
    event = Event.find(params[:event_id])
    AcceptedEventMember.destroy_all :user_id => current_user.id, :event_id => event.id
    redirect_to event
  end
end