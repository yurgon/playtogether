class AdvertsController < ApplicationController
  def index
    @advert = Advert.first || Advert.new
  end

  def update
    @advert = Advert.first


    if @advert.update_attributes(advert_params)
      flash.now[:notice] = 'Данные успешно сохранены'
    else
      flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
    end

    redirect_to adverts_path
  end

  def create
    @advert = Advert.new(advert_params)

    if @advert.save
      flash.now[:notice] = 'Данные успешно сохранены'
      respond_to do |format|
        format.html { redirect_to adverts_path  }
      end
    else
      respond_to do |format|
        flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
        format.html { render action: 'new' }
      end
    end
  end

  def advert_params
    params.require(:advert).permit(:html)
  end
end