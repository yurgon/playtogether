class AdsController < ApplicationController

  before_filter :authenticate_user!

  # GET /ads
  # GET /ads.json
  def index


    @ads = Ad.all


    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /ads/1
  # GET /ads/1.json
  def show
    @ad = Ad.find(params[:id])

    respond_to do |format|
      format.html
    end
  end


  # GET /ads/new
  # GET /ads/new.json
  def new
    @ad = Ad.new

    respond_to do |format|
      format.html
    end
  end

  # GET /ads/1/edit
  def edit
    @ad = Ad.find(params[:id])
  end



  # POST /ads
  # POST /ads.json
  def create
    @ad = Ad.new(ad_params)
    @ad.user_id = current_user.id

    if @ad.save
      flash.now[:notice] = 'Данные успешно сохранены'
      respond_to do |format|
        format.html { redirect_to ad_path(@ad)  }
      end
    else
      respond_to do |format|
        flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
        format.html { render action: 'new' }
      end
    end

  end


  # PUT /ads/1
  # PUT /ads/1.json
  def update
    @ad = Ad.find(params[:id])


    if @ad.update_attributes(ad_params)
      flash.now[:notice] = 'Данные успешно сохранены'
    else
      flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
    end

    respond_to do |format|
      format.html { render action: 'edit' }
      format.js { render action: 'edit' }
    end
  end

  # DELETE /ads/1
  # DELETE /ads/1.json
  def destroy
    @ad = Ad.find(params[:id])
    @ad.destroy

    respond_to do |format|
      format.html { redirect_to ads_url }
      format.json { head :no_content }
    end
  end

  private

  def ad_params
    allowed = [:name, :place_id, :kind, :price, :description ]

    params.require(:ad).permit( *allowed )
  end

end
