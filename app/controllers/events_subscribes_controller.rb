class EventsSubscribesController < ApplicationController

  def create
    current_user.event_subscribes.create :event_id => params[:event_id] unless current_user.event_subscribes.where(:event_id => params[:event_id]).present?
    redirect_to Event.find(params[:event_id])
  end

  def destroy
    current_user.event_subscribes.where(:event_id => params[:event_id]).destroy_all
    redirect_to Event.find(params[:event_id])
  end

end
