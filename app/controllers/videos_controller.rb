class VideosController < ApplicationController

  before_filter :authenticate_user!

  # GET /videos
  # GET /videos.json
  def index

    @videos = Video.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /videos/1
  # GET /videos/1.json
  def show
    @video = Video.find(params[:id])


    @user = User.find(params[:user_id]) if params[:user_id].present?
    @community = Community.find(params[:community_id]) if params[:community_id].present?
    @event = Event.find(params[:event_id]) if params[:event_id].present?
    @spot = Spot.find(params[:spot_id]) if params[:spot_id].present?
    @sport_event = SportEvent.find(params[:sport_event_id]) if params[:sport_event_id].present?

    @videoalbum = Videoalbum.find(params[:videoalbum_id])

    if params[:modal] == 'true'
      respond_to do |format|
              format.html { render layout: false }
            end
    else
      respond_to do |format|
        format.html
      end
    end
  end


  # GET /videos/new
  # GET /videos/new.json
  def new
    @video = Video.new

    respond_to do |format|
      format.html
    end
  end

  # GET /videos/1/edit
  def edit
    @video = Video.find(params[:id])
  end


  # POST /videos
  # POST /videos.json
  def create

    @video = Video.new(video_params)
    @videoalbum = Videoalbum.find(params[:videoalbum_id])
    
    unless params[:videoalbum_id].blank?
      @videoalbum = Videoalbum.find(params[:videoalbum_id])
      @video.videoalbum_id =  @videoalbum.id
    end

    respond_to do |format|
      if @video.save
        if params[:community_id].present?
          format.html { redirect_to community_videoalbum_path @videoalbum.videoalbumable.id, @videoalbum }
          elsif params[:user_id].present?
            format.html { redirect_to user_videoalbum_path @videoalbum.videoalbumable.id, @videoalbum }
          elsif params[:event_id].present?
            format.html { redirect_to event_videoalbum_path @videoalbum.videoalbumable.id, @videoalbum }
          elsif params[:spot_id].present?
            format.html { redirect_to spot_videoalbum_path @videoalbum.videoalbumable.id, @videoalbum }
          elsif params[:sport_event_id].present?
            format.html { redirect_to sport_event_videoalbum_path @videoalbum.videoalbumable.id, @videoalbum }
          elsif params[:ad_id].present?
            format.html { redirect_to ad_videoalbum_path @videoalbum.videoalbumable.id, @videoalbum }
          end

      else
        format.html { render :template => "videoalbums/upload" }
      end
    end

  end


  # PUT /videos/1
  # PUT /videos/1.json
  def update
    @video = Video.find(params[:id])


    if @video.update_attributes(video_params)
      flash.now[:notice] = 'Данные успешно сохранены'
    else
      flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
    end

    respond_to do |format|
      format.html { render action: 'edit' }
      format.js { render action: 'edit' }
    end
  end

  # DELETE /videos/1
  # DELETE /videos/1.json
  def destroy
    @video = Video.find(params[:id])
    @video.destroy

    respond_to do |format|
      format.html { redirect_to user_videos_url current_user }
    end
  end


  private

  def video_params
    allowed = [:video]

    params.require(:video).permit(*allowed)
  end

end
