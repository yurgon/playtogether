class PhotoalbumsController < ApplicationController

  before_filter :authenticate_user!

  # GET /photoalbums
  # GET /photoalbums.json
  def index
    if !params[:user_id].blank?

      @user = User.find(params[:user_id])

      @photoalbums = @user.photoalbums.accessible_by(current_ability).order("created_at DESC")

    elsif !params[:community_id].blank?

        @community = Community.find(params[:community_id])
        @photoalbums = @community.photoalbums.accessible_by(current_ability).order("created_at DESC")
    elsif !params[:event_id].blank?

      @event = Event.find(params[:event_id])
      @photoalbums = @event.photoalbums.order("created_at DESC")
    elsif !params[:spot_id].blank?

      @spot = Spot.find(params[:spot_id])
      @photoalbums = @spot.photoalbums.order("created_at DESC")
    elsif !params[:sport_event_id].blank?

      @sport_event = SportEvent.find(params[:sport_event_id])
      @photoalbums = @sport_event.photoalbums.order("created_at DESC")
    elsif !params[:ad_id].blank?

      @ad = Ad.find(params[:ad_id])
      @photoalbums = @ad.photoalbums.order("created_at DESC")
    end

    render layout: 'profile'
  end

  # GET /photoalbums/1
  # GET /photoalbums/1.json
  def show
    @photoalbum = Photoalbum.find(params[:id])

    unless params[:user_id].blank?

      @user = User.find(params[:user_id])
    end

    unless params[:community_id].blank?

         @community = Community.find(params[:community_id])
    end

    unless params[:event_id].blank?

      @event = Event.find(params[:event_id])
    end
    unless params[:spot_id].blank?

        @spot = Spot.find(params[:spot_id])
    end
    unless params[:sport_event_id].blank?

      @sport_event = SportEvent.find(params[:sport_event_id])
    end
    unless params[:ad_id].blank?

      @ad = Ad.find(params[:ad_id])
    end

    render layout: "profile"
  end

  def upload
    @photoalbum = Photoalbum.find(params[:photoalbum_id])

    unless params[:user_id].blank?

      @user = User.find(params[:user_id])
    end
    unless params[:community_id].blank?

      @community = Community.find(params[:community_id])
    end
    unless params[:event_id].blank?

      @spot = Spot.find(params[:spot_id])
    end
    unless params[:spot_id].blank?

      @spot = Spot.find(params[:spot_id])
    end
    unless params[:sport_event_id].blank?

      @sport_event = SportEvent.find(params[:sport_event_id])

    end
    unless params[:ad_id].blank?

      @ad = Ad.find(params[:ad_id])
    end

    respond_to do |format|
      format.html { render layout: "profile" }
      format.json { render json: {files: [@photoalbum.to_jq_upload]}, status: :created, location: @photoalbum }
    end
  end


  # GET /photoalbums/new
  # GET /photoalbums/new.json
  def new
    @photoalbum = Photoalbum.new

    if !params[:user_id].blank?

      @user = User.find(params[:user_id])
    elsif !params[:community_id].blank?
        @community = Community.find(params[:community_id])
    elsif !params[:event_id].blank?
      @event = Event.find(params[:event_id])
    elsif !params[:spot_id].blank?
      @spot = Spot.find(params[:spot_id])
    elsif !params[:sport_event_id].blank?
      @sport_event = SportEvent.find(params[:sport_event_id])
    elsif !params[:ad_id].blank?
      @ad = Ad.find(params[:ad_id])
    end

    render layout: "profile"
  end

  # GET /photoalbums/1/edit
  def edit

    if !params[:user_id].blank?

      @user = User.find(params[:user_id])
    elsif !params[:community_id].blank?

      @community = Community.find(params[:community_id])
    elsif !params[:event_id].blank?

      @event = Event.find(params[:event_id])
    elsif !params[:spot_id].blank?

      @spot = Spot.find(params[:spot_id])
    elsif !params[:sport_event_id].blank?

      @sport_event = SportEvent.find(params[:sport_event_id])
    elsif !params[:ad_id].blank?
      @ad = Ad.find(params[:ad_id])

    end

    @photoalbum = Photoalbum.find(params[:id])
    render layout: 'profile'
  end


  # POST /photoalbums
  # POST /photoalbums.json
  def create

    @photoalbum = Photoalbum.new(photoalbum_params)

    if !params[:user_id].blank?

      @user = User.find(params[:user_id])
      @photoalbum.photoalbumable = @user

      @photoalbum.save

      redirect_to '/users/'+@photoalbum.photoalbumable_id.to_s+'/photoalbums'

    elsif !params[:community_id].blank?

      @community = Community.find(params[:community_id])
      @photoalbum.photoalbumable = @community

      @photoalbum.save

      redirect_to '/communities/'+@photoalbum.photoalbumable_id.to_s+'/photoalbums'

    elsif !params[:event_id].blank?

      @event = Event.find(params[:event_id])
      @photoalbum.photoalbumable = @event

      @photoalbum.save

      redirect_to '/events/'+@photoalbum.photoalbumable_id.to_s+'/photoalbums'
    elsif !params[:spot_id].blank?

      @spot = Spot.find(params[:spot_id])
      @photoalbum.photoalbumable = @spot

      @photoalbum.save

      redirect_to '/spots/'+@photoalbum.photoalbumable_id.to_s+'/photoalbums'
    elsif !params[:sport_event_id].blank?

      @sport_event = SportEvent.find(params[:sport_event_id])
      @photoalbum.photoalbumable = @sport_event

      @photoalbum.save

      redirect_to '/sport_events/'+@photoalbum.photoalbumable_id.to_s+'/photoalbums'
    elsif !params[:ad_id].blank?

      @ad = Ad.find(params[:ad_id])
      @photoalbum.photoalbumable = @ad

      @photoalbum.save

      redirect_to '/ads/'+@photoalbum.photoalbumable_id.to_s+'/photoalbums'

    end


  end

  def update


    @photoalbum = Photoalbum.find(params[:id])
    photoalbumable = @photoalbum.photoalbumable
    @photoalbum.name = params[:photoalbum][:name]
    @photoalbum.save


    redirect_to photoalbumable

  end


  # DELETE /photoalbums/1
  # DELETE /photoalbums/1.json
  def destroy
    @photoalbum = Photoalbum.find(params[:id])
    photoalbumable = @photoalbum.photoalbumable
    @photoalbum.destroy
    redirect_to photoalbumable

  end

  private

  def photoalbum_params
    allowed = [:name]

    params.require(:photoalbum).permit(*allowed)
  end

end
