class CommunitiesController < ApplicationController

  before_filter :authenticate_user!

  # GET /communites
  # GET /communites.json
  def index

    @h1 = 'Сообщества'
    if params[:type]=='Team' || params[:type]=='team'
      @h1 = 'Мои команды'
      @communities = current_user.communities.where('communities.type="Team"').order('memberships.user_status ASC, memberships.community_status').all
      @communities_all = Community.where('communities.type="Team"').all - @communities
    elsif params[:type]=='Infogroup' || params[:type]=='infogroup'
      @h1 = 'Мои группы'
      @communities = current_user.communities.where('communities.type="Infogroup"').order('memberships.user_status ASC, memberships.community_status ').all
      @communities_all = Community.where('communities.type="Infogroup"').all - @communities
    end
    @user = current_user
    render layout: 'profile'
  end

  def search_comm
    @h1 = 'Сообщества'
    if params[:type]=='Team' || params[:type]=='team'
      type = "Team"
    else
      type = "Infogroup"
    end
      @communities = current_user.communities.where('communities.type="Team"').order('memberships.user_status ASC, memberships.community_status').all
      place_ids = params[:place_id] == "all" ? "all" : Place.find(params[:place_id]).to_child
      if place_ids == "all" || params[:sport_type_id] == "all"
        if place_ids == "all"
           @communities_all = Community.where(type: type, sport_type_id: params[:sport_type_id]).all - @communities
        else
           @communities_all = Community.where(type: type, place_id: place_ids).all - @communities
        end
      else
        @communities_all = Community.where(type: type, place_id: place_ids,sport_type_id: params[:sport_type_id]).all - @communities
      end

    render layout: false
  end

  # GET /communites/1
  # GET /communites/1.json
  def show
    @community = Community.find(params[:id])
    @user = current_user
    render layout: 'profile'
  end


  # GET /communites/new
  # GET /communites/new.json
  def new
    @community = Community.new

    unless params[:type].blank?
      @community.type = params[:type]
    end
    @user = current_user
    render layout: "profile"
  end

  # GET /communites/1/edit
  def edit
    @community = Community.find(params[:id])
  end


  # POST /communites
  # POST /communites.json
  def create
    @community = Community.new(community_params)

    membership = @community.memberships.build
    membership.user_id = current_user.id
    membership.user_status = 1
    membership.community_status = 1
    membership.save

    users_with_roles = @community.community_roles.build
    users_with_roles.user_id = current_user.id
    users_with_roles.role = 'admin'
    users_with_roles.save

    if @community.save
      flash.now[:notice] = 'Данные успешно сохранены'
      respond_to do |format|
        format.html { redirect_to community_path(@community) }
      end
    else
      respond_to do |format|
        flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
        format.html { render action: 'new' }
      end
    end

  end


  # PUT /communites/1
  # PUT /communites/1.json
  def update
    @community = Community.find(params[:id])


    if @community.update_attributes(community_params)
      flash[:notice] = 'Данные успешно сохранены'
      redirect_to community_path(@community)
    else
      flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'

      respond_to do |format|
        format.html { render action: 'edit' }
      end
    end

  end

  # DELETE /communites/1
  # DELETE /communites/1.json
  def destroy
    @community = Community.find(params[:id])
    @community.destroy

    respond_to do |format|
      format.html { redirect_to communites_url }
      format.json { head :no_content }
    end
  end


  def members

    @community = Community.find(params[:id])

    if params[:type]=='team'
      @h1 = 'Состав команды'
      @members = @community.users_with_roles.where('community_roles.role = "member"')
    elsif params[:type]=='admins'
      @h1 = 'Администрация группы'
      @members = @community.users_with_roles.where('community_roles.role = "admin"')
    else
      @h1 = 'Все участники группы'
      @members = @community.users
    end

  end

  def member
    @community = Community.find(params[:id])
    @member = @community.users.where('users.id = ?', params[:user_id])
    not_found unless @member.any?
    @member = @member.first
  end

  def edit_member
    @community = Community.find(params[:id])
    not_found unless @community.is_admin?(current_user.id)
    @member = @community.users.where('users.id = ?', params[:user_id])
    not_found unless @member.any?
    @member = @member.first
    if @community.update_attributes(community_params)
      flash[:notice] = 'Данные успешно сохранены'
      redirect_to community_path(@community)+'/member/'+@member.id.to_s
    else
      flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
      respond_to do |format|
        format.html { render action: 'member' }
      end
    end
  end

  def membership_request

    @community = Community.find(params[:id])

    membership = @community.memberships.where(user_id: current_user.id).first_or_initialize
    @community.community_roles.each{|u|
      # Emailer.invite_to_group(current_user,User.find(u.user_id).email,@community).deliver
    }
    membership.user_status = 1
    membership.save


  end


  def invite

    @community = Community.find(params[:id])

    @friends = current_user.friends_users
    render layout: 'profile'
  end

  def membership_invite

    @community = Community.find(params[:id])

    @user = User.find(params[:user_id])

    membership = @community.memberships.where(user_id: @user.id).first_or_initialize
    membership.community_status = 1
    membership.save

  end

  def membership_cancel

    @community = Community.find(params[:id])

    unless params[:user_id].blank?
      user = User.find(params[:user_id])
    else
      user = current_user
    end

    membership = @community.memberships.where(user_id: user.id).first_or_initialize

    if user.id == current_user.id
      membership.destroy
    else
      membership.community_status = -1
    end

  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def get_sports
    @sport_type = SportType.where(parent_id: params[:id])
    respond_to do |format|
      format.json { render :json => @sport_type.to_json, :status => 200 }
    end
  end

  private

  def community_params
    allowed = [:name, :about, :type, :avatar, :place_id, :sport_type_id,
               community_roles_attributes: [:id, :role, :role_description, :number, :user_id, :_destroy, :status],
               team_player_searches_attributes: [:id, :level, :sport_type_id, :_destroy]
    ]

    params.require(:community).permit(*allowed)
  end

end
