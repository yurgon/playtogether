class PhotosController < ApplicationController

  before_filter :authenticate_user!

  # GET /photos
  # GET /photos.json
  def index


    @photos = Photo.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @photos.map { |upload| upload.to_jq_upload } }
    end
  end

  # GET /photos/1
  # GET /photos/1.json
  def show
    @photo = Photo.find(params[:id])

    @user = User.find(params[:user_id]) if params[:user_id].present?
    @community = Community.find(params[:community_id]) if params[:community_id].present?
    @event = Event.find(params[:event_id]) if params[:event_id].present?
    @spot = Spot.find(params[:spot_id]) if params[:spot_id].present?
    @sport_event = SportEvent.find(params[:sport_event_id]) if params[:sport_event_id].present?
    @ad = Ad.find(params[:ad_id]) if params[:ad_id].present?

    @photoalbum = Photoalbum.find(params[:photoalbum_id])

    if params[:modal] == 'true'
      respond_to do |format|
              format.html { render layout: false }
            end
    else
      respond_to do |format|
        format.html
      end
    end


  end


  # GET /photos/new
  # GET /photos/new.json
  def new
    @photo = Photo.new

    respond_to do |format|
      format.html
    end
  end

  # GET /photos/1/edit
  def edit
    @photo = Photo.find(params[:id])
  end


  # POST /photos
  # POST /photos.json
  def create

    p_attr = params[:photo]
    p_attr[:photo] = params[:photo][:photo].first if params[:photo][:photo].class == Array


    @photo = Photo.new
    @photo.photo = p_attr[:photo]
    @photo.user_id = current_user.id


    unless params[:photoalbum_id].blank?
      @photoalbum = Photoalbum.find(params[:photoalbum_id])
      @photo.photoalbum_id =  @photoalbum.id
    end

    respond_to do |format|
      if @photo.save
        format.html {
          render :json => [@photo.to_jq_upload].to_json,
                 :content_type => 'text/html',
                 :layout => false
        }
        format.json { render json: {files: [@photo.to_jq_upload]}, status: :created }
      else
        format.html { render action: "new" }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end

  end


  # PUT /photos/1
  # PUT /photos/1.json
  def update
    @photo = Photo.find(params[:id])


    if @photo.update_attributes(photo_params)
      flash.now[:notice] = 'Данные успешно сохранены'
    else
      flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
    end

    respond_to do |format|
      format.html { render action: 'edit' }
      format.js { render action: 'edit' }
    end
  end

  # DELETE /photos/1
  # DELETE /photos/1.json
  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy

    respond_to do |format|
      format.html { redirect_to photos_url }
      format.json { head :no_content }
    end
  end


  private

  def photo_params
    allowed = [:photo, :description]

    params.require(:photo).permit(*allowed)
  end

end
