class MembersRequestsController < ApplicationController
  def create
    community = Community.find(params['community_id'])
    MembersRequest.create user_id:current_user.id, community_id:community.id unless MembersRequest.exists? user_id:current_user.id, community_id:community.id
    redirect_to community
  end

  def destroy
    request = MembersRequest.find params['request_id']
    request.destroy
  end

  def index
    @community = Community.find params['community_id']
    @requests = MembersRequest.where :community_id => params['community_id']
  end

  def add
    request = MembersRequest.find params['request_id']
    user_id = request.user_id
    community_id = request.community_id
    CommunityRole.create :role => 'member', :user_id => user_id, :community_id => community_id
    request.destroy
    redirect_to index_member_request_path(community_id)
  end
end