class SportEventsController < ApplicationController

  before_filter :authenticate_user!

  # GET /sport_events
  # GET /sport_events.json
  def index


    @sport_events = SportEvent.page(params[:page]).per(10).order('date DESC')

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /sport_events/1
  # GET /sport_events/1.json
  def show
    @sport_event = SportEvent.find(params[:id])

    respond_to do |format|
      format.html
    end
  end


  # GET /sport_events/new
  # GET /sport_events/new.json
  def new
    @sport_event = SportEvent.new

    respond_to do |format|
      format.html
    end
  end

  # GET /sport_events/1/edit
  def edit
    @sport_event = SportEvent.find(params[:id])
  end



  # POST /sport_events
  # POST /sport_events.json
  def create
    @sport_event = SportEvent.new(sport_event_params)

    if @sport_event.save
      flash.now[:notice] = 'Данные успешно сохранены'
      respond_to do |format|
        format.html { redirect_to edit_sport_event_path(@sport_event)  }
      end
    else
      respond_to do |format|
        flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
        format.html { render action: 'new' }
      end
    end

  end


  # PUT /sport_events/1
  # PUT /sport_events/1.json
  def update
    @sport_event = SportEvent.find(params[:id])


    if @sport_event.update_attributes(sport_event_params)
      flash.now[:notice] = 'Данные успешно сохранены'
      redirect_to sport_event_path @sport_event
      return
    else
      flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
    end

    respond_to do |format|
      format.html { render action: 'edit' }
      format.js { render action: 'edit' }
    end
  end

  # DELETE /sport_events/1
  # DELETE /sport_events/1.json
  def destroy
    @sport_event = SportEvent.find(params[:id])
    @sport_event.destroy

    respond_to do |format|
      format.html { redirect_to sport_events_url }
      format.json { head :no_content }
    end
  end

  private

  def sport_event_params
    allowed = [:image, :header, :announce, :content, :date]

    params.require(:sport_event).permit( *allowed )
  end

end
