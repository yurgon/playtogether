class MessagesController < ApplicationController

  before_filter :authenticate_user!

  # GET /messages
  # GET /messages.json
  def index
    @messages = Message.accessible_by(current_ability).order("created_at DESC")
    @user = current_user
    render layout: "profile"
  end

  # GET /messages/1
  # GET /messages/1.json
  def show
    @message = Message.find(params[:id])

    respond_to do |format|
      format.html
    end
  end


  # GET /messages/new
  # GET /messages/new.json
  def new
    @message = Message.new

    respond_to do |format|
      format.html
    end
  end

  # GET /messages/1/edit
  def edit
    @message = Message.find(params[:id])
  end


  # POST /messages
  # POST /messages.json
  def create


    @message = Message.new(message_params)

    @message.sender_id = current_user.id

    unless @message.content.blank?
      @message.save
    end
    # Emailer.input_messages(current_user,User.find(message_params[:receiver_id]).email,@message).deliver
    redirect_to '/users/'+@message.receiver_id.to_s+'/messages#send'

  end


  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    @message = Message.find(params[:id])

    if @message.sender_id == current_user.id


      if @message.receiver_status = -1
        @message.destroy
      else
        @message.sender_status = -1
        @message.save

      end


      redirect_to '/users/'+@message.receiver_id.to_s+'/messages#send'

    elsif @message.receiver_id == current_user.id

      if @message.sender_status = -1
        @message.destroy
      else
        @message.receiver_status = -1
        @message.save
      end

      redirect_to '/users/'+@message.sender_id.to_s+'/messages#send'

    end


  end

  private

  def message_params
    allowed = [:id, :receiver_id, :content]

    params.require(:message).permit(*allowed)
  end

end
