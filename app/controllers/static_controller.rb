class StaticController < ApplicationController

  def index

    unless current_user.nil?
      redirect_to user_path(current_user)
    end
  end

  def about
    if user_signed_in?
      render layout: "profile"
    else
      render layout: 'text_pages'
    end
  end

  def features
    if user_signed_in?
      render layout: "profile"
    else
      render layout: 'text_pages'
    end
  end

  def offerta
    render layout: 'text_pages'
  end

  def advert
    if user_signed_in?
      render layout: "profile"
    else
      render layout: 'text_pages'
    end
  end



end
