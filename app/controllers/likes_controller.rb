class LikesController < ApplicationController
  def like
    if params[:type].present? and params[:id].present?
      Like.where(likeable_type: params[:type],likeable_id: params[:id], user_id: current_user.id).first_or_create
    end
  end

  def unlike
    if params[:type].present? and params[:id].present?
      Like.where(likeable_type: params[:type],likeable_id: params[:id], user_id: current_user.id).first.destroy
    end
  end

  def plus_minus
    if params[:type].present? and params[:id].present?
      record = Like.where(likeable_type: params[:type], likeable_id: params[:id], user_id: current_user.id).first
      if record
        likeable = record.likeable
        record.destroy
      else
        like = Like.create(likeable_type: params[:type], likeable_id: params[:id], user_id: current_user.id)
        likeable = like.likeable
      end
    end
    render text: likeable.likes.count
  end


end
