class FavoritesController < ApplicationController
  def favorite
    if params[:type].present? and params[:id].present?
      favorite = Favorite.where(favoritable_type: params[:type], favoritable_id: params[:id], user_id: current_user.id).first_or_create
      @model = favorite.favoritable
    end
  end

  def unfavorite
    if params[:type].present? and params[:id].present?
      favorite = Favorite.where(favoritable_type: params[:type], favoritable_id: params[:id], user_id: current_user.id).first_or_initialize
      @model = favorite.favoritable
      favorite.destroy
    end
  end

  def index

    @favorites = Favorite.where(user_id: current_user.id).order('created_at DESC')


  end
end
