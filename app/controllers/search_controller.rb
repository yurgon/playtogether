class SearchController < ApplicationController

  def users
    @users = User.search(params).limit(50)
    render layout: 'profile'
  end

  def infogroups
    @infogroups = Infogroup.search(params).limit(50)
    render layout: 'profile'
  end

  def teams
    @teams = Team.search(params).limit(50)
    render layout: 'profile'
  end

  def spots
    @spots = Spot.search(params).limit(50)
    render layout: 'profile'
  end

  def events
    @events = Event.search(params).limit(50)
    render layout: 'profile'
  end


end
