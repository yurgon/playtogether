class SpotsController < ApplicationController

  before_filter :authenticate_user!

  # GET /spots
  # GET /spots.json
  def index


    @spots = Spot.all

    render layout: 'profile'
  end

  # GET /spots/1
  # GET /spots/1.json
  def show
    @spot = Spot.find(params[:id])
    @user = current_user
    render layout: 'profile'
  end


  # GET /spots/new
  # GET /spots/new.json
  def new
    @spot = Spot.new
    render layout: "profile"
  end

  # GET /spots/1/edit
  def edit
    @spot = Spot.find(params[:id])
    render layout: "profile"
  end



  # POST /spots
  # POST /spots.json
  def create
    @spot = Spot.new(spot_params)
    @spot.user_id = current_user.id

    if @spot.save
      flash.now[:notice] = 'Данные успешно сохранены'
      respond_to do |format|
        format.html { redirect_to spot_path(@spot)  }
      end
    else
      respond_to do |format|
        flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
        format.html { render action: 'new' }
      end
    end

  end


  # PUT /spots/1
  # PUT /spots/1.json
  def update
    @spot = Spot.find(params[:id])


    if @spot.update_attributes(spot_params)
      flash.now[:notice] = 'Данные успешно сохранены'
    else
      flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
    end

    respond_to do |format|
      format.html { render action: 'edit' }
      format.js { render action: 'edit' }
    end
  end

  # DELETE /spots/1
  # DELETE /spots/1.json
  def destroy
    @spot = Spot.find(params[:id])
    @spot.destroy

    respond_to do |format|
      format.html { redirect_to spots_url }
      format.json { head :no_content }
    end
  end

  private

  def spot_params
    allowed = [:name, :place_id, :kind, :paid, :about, :address ]

    params.require(:spot).permit( *allowed )
  end

end
