class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :set_access_time
  before_filter :set_language
  def after_sign_in_path_for(resource_or_scope)
      user_path(current_user.id)
  end

  def after_sign_out_path_for(resource_or_scope)
    "/"
  end
  def set_access_time
    unless current_user.nil?

      if current_user.online_time.nil? || current_user.online_time + 1.minute < DateTime.now

        current_user.online_time =  DateTime.now
        current_user.save

      end

    end
  end

  def set_language
    if params['language'] && current_user
      if params['language'] == 'rus'
        current_user.language = 'rus'
      else
        current_user.language = 'eng'
      end
      current_user.save
    end
  end

end
