class EventsController < ApplicationController

  before_filter :authenticate_user!

  # GET /events
  # GET /events.json
  def index
    @events = Event.all
    @user = current_user
    render layout: 'profile'
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @event = Event.find(params[:id])
    @invites = @event.invites
    @subscribes = @event.event_subscribes
    @user = current_user
    render layout: "profile"
  end

  def search_event
      place_ids = params[:place_id_event] == "all" ? "all" : Place.find(params[:place_id_event]).to_child

      if place_ids == "all" && params[:date] == "all"
         @events = Event.all
      else
        if place_ids == "all" || params[:date] == "all"
          if params[:date] == "all"
             @events = Event.where(place_id: place_ids).all
          else
             a=params[:date].split("-")
             @events = Event.where("date > ?", Time.new(a[0],a[1],a[2])).all
          end
        else
          a=params[:date].split("-")
          @events = Event.where("place_id in(?) and date > ?",place_ids.join(","),Time.new(a[0],a[1],a[2])).all
        end
      end

    render layout: false

  end
  # GET /events/new
  # GET /events/new.json
  def new
    @event = Event.new
    @user = current_user
    render layout: 'profile'
  end

  # GET /events/1/edit
  def edit
    @event = Event.find(params[:id])
    @user = current_user
    render layout: 'profile'
  end



  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.user_id = current_user.id
    if @event.save
      flash.now[:notice] = 'Данные успешно сохранены'
      respond_to do |format|
        format.html { redirect_to event_path(@event)  }
      end
    else
      respond_to do |format|
        flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
        format.html { render action: 'new' }
      end
    end

  end


  # PUT /events/1
  # PUT /events/1.json
  def update
    @event = Event.find(params[:id])

    if @event.update_attributes(event_params)
      flash.now[:notice] = 'Данные успешно сохранены'
    else
      flash.now[:error] = 'Данные не сохранены. Проверьте введенные данные на наличие ошибок.'
    end

    respond_to do |format|
      format.html { redirect_to event_path(@event) }
      format.js { render action: 'edit' }
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event = Event.find(params[:id])
    @event.destroy

    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :no_content }
    end
  end

  def my_events
    event_ids = AcceptedEventMember.where(:user_id => current_user).pluck(:event_id)
    @events = Event.where :id => event_ids

    render layout: 'profile'
  end

  private

  def event_params
    allowed = [ :place_id, :date, :description, :kind, :name, :team1_id, :team2_id, :user_id, :spot_id, :invite_team1_id, :invite_team2_id, :result ]

    params.require(:event).permit( *allowed )
  end

end
