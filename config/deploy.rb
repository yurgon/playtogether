require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
# require 'mina/rbenv'  # for rbenv support. (http://rbenv.org)
require 'mina/rvm'    # for rvm support. (http://rvm.io)
#require 'mina/puma'
#require 'mina/rsync'

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)

set :site, 'skidler.com'
set :domain, '185.4.74.209'
set :port, 5555
set :deploy_to, "/var/www/#{site}"
set :repository, 'git@bitbucket.org:yurgon/playtogether.git'
set :branch, 'master'
set :rvm_path, '/usr/local/rvm/bin/rvm'
set :bundle_path, '~/bundle'
set :use_sudo, true

# Manually create these paths in shared/ (eg: shared/config/database.yml) in your server.
# They will be linked in the 'deploy:link_shared_paths' step.
set :shared_paths, ['config/database.yml', 'log', 'config/secrets.yml', 'public/uploads']

set :bundle_options, lambda { %{--without development:test --path "#{bundle_path}" --deployment} }

# Optional settings:
#   set :user, 'foobar'    # Username in the server to SSH to.
#   set :port, '30000'     # SSH port number.

# This task is the environment that is loaded for most commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  #invoke :'rbenv:load'
  invoke :'rvm:use[ruby-2.1.3-p242]'
end

# Put any custom mkdir's in here for when `mina setup` is ran.
# For Rails apps, we'll make some of the shared paths that are shared between
# all releases.
task :setup => :environment do

  queue! %[mkdir -p "#{deploy_to}"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}"]

  queue! %[mkdir -p "#{deploy_to}/shared/log"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/log"]

  queue! %[mkdir -p "#{deploy_to}/shared/config"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/config"]

  queue! %[touch "#{deploy_to}/shared/config/database.yml"]
  queue  %[echo "-----> Be sure to edit 'shared/config/database.yml'."]

  queue! %[touch "#{deploy_to}/shared/config/secrets.yml"]
  queue  %[echo "-----> Be sure to edit 'shared/config/secrets.yml'."]

  queue! %[mkdir -p "#{deploy_to}/shared/public/uploads"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/public/uploads"]


end

desc "Deploys the current version to the server."
task :deploy => :environment do
  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'

  end
end



task :import => :environment do
  queue "cd #{deploy_to}/current ; bundle exec rake import:products RAILS_ENV=production"
end

task :seed => :environment do
  queue "cd #{deploy_to}/current ; bundle exec rake db:seed RAILS_ENV=production"
end

task :start => :environment do
  queue "cp #{deploy_to}/current/config/nginx.conf #{deploy_to}/shared/config/"
  queue "rm #{deploy_to}/current/config/nginx.conf"
  queue "sudo ln #{deploy_to}/shared/config/nginx.conf /etc/nginx/sites-enabled/#{site}-nginx.conf"
  queue "sudo service nginx restart"
end


# For help in making your deploy script, see the Mina documentation:
#
#  - http://nadarei.co/mina
#  - http://nadarei.co/mina/tasks
#  - http://nadarei.co/mina/settings
#  - http://nadarei.co/mina/helpers

