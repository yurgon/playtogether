crumb :root do
  link '<i class="fa fa-home"></i>'.html_safe, root_path
end

crumb :user do |user|
  link user.name, user_path(user)
end
crumb :friends do |user|
  link 'Друзья', friends_user_path
  parent :user, user
end
crumb :message do |user|
  link 'Диалог', messages_user_path
  parent :user, user
end
crumb :user_videoalbums do |user|
  link 'Видеоальбомы', '/users/'+user.id.to_s+'/videoalbums/'
  parent :user, user
end
crumb :user_photoalbums do |user|
  link 'Фотооальбомы', '/users/'+user.id.to_s+'/photoalbums/'
  parent :user, user
end
crumb :user_photoalbum do |photoalbum,user|
  link 'Фотооальбом'+photoalbum.name, '/users/'+user.id.to_s+'/photoalbums/'+photoalbum.id.to_s
  parent :user_photoalbums, user
end
crumb :user_videoalbum do |videoalbum,user|
  link 'Видеоальбом'+videoalbum.name, '/users/'+user.id.to_s+'/videoalbums/'+videoalbum.id.to_s
  parent :user_videoalbums, user
end


crumb :community do |community|
  link community.name, community_path(community)
end
crumb :community_videoalbums do |community|
  link 'Видеоальбомы', '/communities/'+community.id.to_s+'/videoalbums/'
  parent :community, community
end
crumb :community_photoalbums do |community|
  link 'Фотооальбомы', '/communities/'+community.id.to_s+'/photoalbums/'
  parent :community, community
end
crumb :community_photoalbum do |photoalbum,community|
  link 'Фотооальбом'+photoalbum.name, '/communities/'+community.id.to_s+'/photoalbums/'+photoalbum.id.to_s
  parent :community_photoalbums, community
end
crumb :community_videoalbum do |videoalbum,community|
  link 'Видеоальбом'+videoalbum.name, '/communities/'+community.id.to_s+'/videoalbums/'+videoalbum.id.to_s
  parent :community_videoalbums, community
end

crumb :community_members do |community|
  link 'Участники команды', community_path(community)+'/members/'
  parent :community, community
end


crumb :event do |event|
  link event.name, event_path(event)
end
crumb :event_videoalbums do |event|
  link 'Видеоальбомы', '/events/'+event.id.to_s+'/videoalbums/'
  parent :event, event
end
crumb :event_photoalbums do |event|
  link 'Фотооальбомы', '/events/'+event.id.to_s+'/photoalbums/'
  parent :event, event
end
crumb :event_photoalbum do |photoalbum,event|
  link 'Фотооальбом'+photoalbum.name, '/events/'+event.id.to_s+'/photoalbums/'+photoalbum.id.to_s
  parent :event_photoalbums, event
end
crumb :event_videoalbum do |videoalbum,event|
  link 'Видеоальбом'+videoalbum.name, '/communities/'+event.id.to_s+'/videoalbums/'+videoalbum.id.to_s
  parent :event_videoalbums, event
end

crumb :event_members do |event|
  link 'Участники мироприятия', event_path(community)+'/members/'
  parent :event, event
end


crumb :spot do |spot|
  link spot.name, spot_path(spot)
end
crumb :spot_videoalbums do |spot|
  link 'Видеоальбомы', '/spots/'+spot.id.to_s+'/videoalbums/'
  parent :spot, spot
end
crumb :spot_photoalbums do |spot|
  link 'Фотооальбомы', '/spots/'+spot.id.to_s+'/photoalbums/'
  parent :spot, spot
end
crumb :spot_photoalbum do |photoalbum,spot|
  link 'Фотооальбом'+photoalbum.name, '/spots/'+spot.id.to_s+'/photoalbums/'+photoalbum.id.to_s
  parent :spot_photoalbums, spot
end
crumb :spot_videoalbum do |videoalbum,spot|
  link 'Видеоальбом'+videoalbum.name, '/spots/'+spot.id.to_s+'/videoalbums/'+videoalbum.id.to_s
  parent :spot_videoalbums, spot
end

crumb :spot_members do |spot|
  link 'Участники мироприятия', spot_path(spot)+'/members/'
  parent :spot, spot
end


crumb :sport_event do |sport_event|
  link sport_event.announce || 'Новость', sport_event_path(sport_event)
end
crumb :sport_event_videoalbums do |sport_event|
  link 'Видеоальбомы', '/sport_events/'+sport_event.id.to_s+'/videoalbums/'
  parent :sport_event, sport_event
end
crumb :sport_event_photoalbums do |sport_event|
  link 'Фотооальбомы', '/sport_events/'+sport_event.id.to_s+'/photoalbums/'
  parent :sport_event, sport_event
end
crumb :sport_event_photoalbum do |photoalbum,sport_event|
  link 'Фотооальбом'+photoalbum.name, '/sport_events/'+sport_event.id.to_s+'/photoalbums/'+photoalbum.id.to_s
  parent :sport_event_photoalbums, sport_event
end
crumb :sport_event_videoalbum do |videoalbum,sport_event|
  link 'Видеоальбом'+videoalbum.name, '/sport_events/'+sport_event.id.to_s+'/videoalbums/'+videoalbum.id.to_s
  parent :sport_event_videoalbums, sport_event
end

crumb :sport_event_members do |sport_event|
  link 'Участники мироприятия', sport_event_path(spot)+'/members/'
  parent :sport_event, sport_event
end


crumb :ad do |ad|
  link ad.name, ad_path(ad)
end
crumb :ad_videoalbums do |ad|
  link 'Видеоальбомы', '/ads/'+ad.id.to_s+'/videoalbums/'
  parent :ad, ad
end
crumb :ad_photoalbums do |ad|
  link 'Фотооальбомы', '/ads/'+ad.id.to_s+'/photoalbums/'
  parent :ad, ad
end
crumb :ad_photoalbum do |photoalbum,ad|
  link 'Фотооальбом'+photoalbum.name, '/ads/'+ad.id.to_s+'/photoalbums/'+photoalbum.id.to_s
  parent :ad_photoalbums, ad
end
crumb :ad_videoalbum do |videoalbum,ad|
  link 'Видеоальбом'+videoalbum.name, '/ads/'+ad.id.to_s+'/videoalbums/'+videoalbum.id.to_s
  parent :ad_videoalbums, ad
end

crumb :ad_members do |ad|
  link 'Участники мироприятия', ad_path(ad)+'/members/'
  parent :ad, ad
end

# crumb :projects do
#   link "Projects", projects_path
# end

# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).