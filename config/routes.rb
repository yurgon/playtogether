Playtogether::Application.routes.draw do

  resources :key_values

  namespace :admin do
    resources :users do
      member do
        get 'user_ban'
      end
    end


    resources :photoalbums do
      resources :photos
      get 'upload'
    end

    resources :photos do
      get 'upload'
    end

    resources :posting_updates do
    end

    resources :places do
      member do
        get 'spots_for_select'
        get 'children_nodes'
      end
    end

    resources :sport_types do
      member do
        get 'children_nodes'
      end
    end
  end

  get 'likes/like'
  get 'likes/unlike'
  get 'likes/plus_minus'
  get 'retweets/create'

  match  'favorites' => 'favorites#index'
  get 'favorites/favorite'
  get 'favorites/unfavorite'

  match  'invites' => 'Invites#index'
  get 'invites/accept/:invite_id', :action => 'accept', :controller => 'invites', :as => 'accept_invite'
  get 'invites/cancel/:invite_id', :action => 'cancel', :controller => 'invites', :as => 'cancel_invite'

  get 'events_subscribes/create/:event_id', :action => 'create', :controller => 'events_subscribes', :as => 'subscribe_event'
  get 'events_subscribes/destroy/:event_id', :action => 'destroy', :controller => 'events_subscribes', :as => 'unsubscribe_event'
  get 'events_accept_members/create/:event_id', :action => 'create', :controller => 'accepted_event_members', :as => 'accept_member_event'
  get 'events_accept_members/destroy/:event_id', :action => 'destroy', :controller => 'accepted_event_members', :as => 'decline_member_event'

  get 'members_request/create/:community_id', :action => 'create', :controller => 'members_requests', :as => 'create_member_request'
  get 'members_request/destroy/:request_id', :action => 'destroy', :controller => 'members_requests', :as => 'destroy_member_request'
  get 'members_request/add/:request_id', :action => 'add', :controller => 'members_requests', :as => 'add_member_request'
  get 'members_request/index/:community_id', :action => 'index', :controller => 'members_requests', :as => 'index_member_request'

  get 'notifies/:community_id', to: 'notifies#new', :as => :notify_new
  post 'notifies/create/:community_id', to: 'notifies#create', :as => :notify_create

  post 'support/create/', to: 'support#create', :as => :support_create

  get 'my_events', to: 'events#my_events', :as => :my_events

  get 'search/users'
  get 'search/infogroups'
  get 'search/teams'
  get 'search/photos'
  get 'search/videos'
  get 'search/spots'
  get 'search/events'

  resources :adverts
  resources :users do
    member do
      get 'friends'
      get 'search_friend'
      get 'add_to_friends'
      get 'remove_from_friends'
      get 'messages'
      get 'communities'
      post 'invite_to_friend'
    end
    resources :photoalbums do
      resources :photos
      get 'upload'
    end
    resources :videoalbums do
      resources :videos
      get 'upload'
    end

  end
  resources :comments
  get 'search_event' => 'events#search_event'
  resources :events do
    resources :photoalbums do
      resources :photos
      get 'upload'
    end
    resources :videoalbums do
      resources :videos
      get 'upload'
    end
  end
  resources :messages
  resources :sport_events do
    resources :photoalbums do
      resources :photos
      get 'upload'
    end
    resources :videoalbums do
      resources :videos
      get 'upload'
    end
  end
  resources :ads do
    resources :photoalbums do
      resources :photos
      get 'upload'
    end
    resources :videoalbums do
      resources :videos
      get 'upload'
    end
  end

  get "search_comm" => "communities#search_comm"
  resources :communities do
    member do
      get 'members'
      get 'member/:user_id' , :action => 'member'
      put 'member/:user_id' , :action => 'edit_member'
      get 'membership_request'
      get 'invite'
      get 'membership_invite/:user_id' , :action => 'membership_invite'
      get 'membership_cancel'
    end

    resources :photoalbums do
      resources :photos
      get 'upload'
    end
    resources :videoalbums do
      resources :videos
      get 'upload'
    end
  end
  get '/communities/get_sports/:id' => 'communities#get_sports'

  resources :spots do
    resources :photoalbums do
      resources :photos
      get 'upload'
    end
    resources :videoalbums do
      resources :videos
      get 'upload'
    end
  end

  devise_for :users,
             :controllers => {
                 :passwords => 'users/passwords',
                 :registrations => 'users/registrations',
                 :sessions => 'users/sessions'
             },
             :path => '',
             :path_names => {
                 :sign_in => 'login', :sign_out => 'logout',
                 :password => 'password', :confirmation => 'confirmation',
                 :unlock => 'unlock',
                 :sign_up => 'registration'
             }

  match 'images/:id/:name' => 'images#download'




  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with 'root'
  # just remember to delete public/index.html.
  match 'about' => 'static#about'
  match 'features' => 'static#features'
  match 'offerta' => 'static#offerta'
  match 'advert' => 'static#advert'
  devise_scope :user do
    root :to => 'devise/sessions#new'
  end

  # See how all your routes lay out with 'rake routes'

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
