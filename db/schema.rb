# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150407074419) do

  create_table "accepted_event_members", :force => true do |t|
    t.integer  "user_id"
    t.integer  "team_id"
    t.integer  "event_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "accepted_event_members", ["event_id"], :name => "index_accepted_event_members_on_event_id"
  add_index "accepted_event_members", ["user_id"], :name => "index_accepted_event_members_on_user_id"

  create_table "ads", :force => true do |t|
    t.string   "name"
    t.integer  "kind"
    t.text     "description"
    t.integer  "price"
    t.integer  "place_id"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "ads", ["place_id"], :name => "index_ads_on_place_id"
  add_index "ads", ["user_id"], :name => "index_ads_on_user_id"

  create_table "adverts", :force => true do |t|
    t.text     "html"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "comments", :force => true do |t|
    t.string   "commentable_type"
    t.integer  "commentable_id"
    t.integer  "user_id"
    t.datetime "date"
    t.text     "content"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "retweetable_type"
    t.integer  "retweetable_id"
  end

  add_index "comments", ["commentable_id", "commentable_type"], :name => "commentable"
  add_index "comments", ["retweetable_id", "retweetable_type"], :name => "index_comments_on_retweetable_id_and_retweetable_type"
  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"

  create_table "communities", :force => true do |t|
    t.string   "type"
    t.string   "name"
    t.text     "about"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "avatar"
    t.integer  "place_id"
    t.integer  "sport_type_id"
  end

  add_index "communities", ["id", "type"], :name => "index_communities_on_id_and_type"
  add_index "communities", ["place_id"], :name => "index_communities_on_place_id"
  add_index "communities", ["sport_type_id"], :name => "index_communities_on_sport_type_id"

  create_table "community_roles", :force => true do |t|
    t.integer  "user_id"
    t.integer  "community_id"
    t.string   "role"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "role_description"
    t.integer  "number"
    t.string   "status"
  end

  add_index "community_roles", ["community_id", "user_id"], :name => "community_user"
  add_index "community_roles", ["community_id"], :name => "index_community_roles_on_community_id"
  add_index "community_roles", ["user_id"], :name => "index_community_roles_on_user_id"

  create_table "event_subscribes", :force => true do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "event_subscribes", ["event_id"], :name => "index_event_subscribes_on_event_id"
  add_index "event_subscribes", ["user_id"], :name => "index_event_subscribes_on_user_id"

  create_table "events", :force => true do |t|
    t.string   "name"
    t.datetime "date"
    t.integer  "kind"
    t.text     "description"
    t.integer  "user_id"
    t.integer  "team1_id"
    t.integer  "team2_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "place_id"
    t.integer  "spot_id"
    t.string   "result"
  end

  add_index "events", ["place_id"], :name => "index_events_on_place_id"
  add_index "events", ["user_id"], :name => "index_events_on_user_id"

  create_table "favorites", :force => true do |t|
    t.integer  "favoritable_id"
    t.string   "favoritable_type"
    t.integer  "user_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "favorites", ["favoritable_id", "favoritable_type"], :name => "favoritable"

  create_table "friends", :force => true do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.integer  "status"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "friends", ["friend_id", "user_id"], :name => "index_friends_on_friend_id_and_user_id"
  add_index "friends", ["friend_id"], :name => "index_friends_on_friend_id"
  add_index "friends", ["user_id", "friend_id"], :name => "index_friends_on_user_id_and_friend_id"
  add_index "friends", ["user_id"], :name => "index_friends_on_user_id"

  create_table "invites", :force => true do |t|
    t.integer  "event_id"
    t.integer  "community_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "admin_id"
  end

  add_index "invites", ["community_id"], :name => "index_invites_on_community_id"
  add_index "invites", ["event_id"], :name => "index_invites_on_event_id"

  create_table "key_values", :force => true do |t|
    t.string   "key"
    t.text     "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "likes", :force => true do |t|
    t.integer  "user_id"
    t.string   "likeable_type"
    t.string   "likeable_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "members_requests", :force => true do |t|
    t.integer  "user_id"
    t.integer  "community_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "members_requests", ["community_id"], :name => "index_members_requests_on_community_id"
  add_index "members_requests", ["user_id"], :name => "index_members_requests_on_user_id"

  create_table "memberships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "community_id"
    t.integer  "user_status"
    t.integer  "community_status"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "memberships", ["community_id", "user_id"], :name => "community_user"
  add_index "memberships", ["community_id"], :name => "index_memberships_on_community_id"
  add_index "memberships", ["user_id", "community_id"], :name => "user_community"
  add_index "memberships", ["user_id"], :name => "index_memberships_on_user_id"

  create_table "messages", :force => true do |t|
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.text     "content"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "receiver_status"
    t.integer  "sender_status"
  end

  add_index "messages", ["receiver_id"], :name => "index_messages_on_receiver_id"
  add_index "messages", ["sender_id"], :name => "index_messages_on_sender_id"

  create_table "notices", :force => true do |t|
    t.string   "theme"
    t.integer  "user_id"
    t.text     "text"
    t.string   "type"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "notices", ["id", "type"], :name => "index_notices_on_id_and_type"

  create_table "occupations", :force => true do |t|
    t.string   "user_id"
    t.string   "name"
    t.string   "description"
    t.string   "kind"
    t.integer  "month_start"
    t.integer  "year_start"
    t.integer  "month_finish"
    t.integer  "year_finish"
    t.integer  "place_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "occupations", ["place_id"], :name => "index_occupations_on_place_id"
  add_index "occupations", ["user_id"], :name => "index_occupations_on_user_id"

  create_table "photoalbums", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "photoalbumable_id"
    t.string   "photoalbumable_type"
  end

  add_index "photoalbums", ["photoalbumable_id", "photoalbumable_type"], :name => "photoalbumable"

  create_table "photos", :force => true do |t|
    t.integer  "photoalbum_id"
    t.datetime "date"
    t.string   "photo"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "photos", ["photoalbum_id"], :name => "index_photos_on_photoalbum_id"
  add_index "photos", ["user_id"], :name => "index_photos_on_user_id"

  create_table "places", :force => true do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "places", ["parent_id"], :name => "index_places_on_parent_id"

  create_table "sport_events", :force => true do |t|
    t.string   "header"
    t.string   "announce"
    t.text     "content"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.datetime "date"
    t.string   "image"
  end

  create_table "sport_types", :force => true do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sport_types", ["parent_id"], :name => "index_sport_types_on_parent_id"

  create_table "spots", :force => true do |t|
    t.string   "name"
    t.string   "kind"
    t.string   "paid"
    t.integer  "place_id"
    t.text     "size"
    t.text     "about"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "address"
  end

  add_index "spots", ["place_id"], :name => "index_spots_on_place_id"
  add_index "spots", ["user_id"], :name => "index_spots_on_user_id"

  create_table "team_player_searches", :force => true do |t|
    t.integer  "community_id"
    t.integer  "sport_type_id"
    t.integer  "level"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "team_player_searches", ["community_id"], :name => "index_team_player_searches_on_community_id"
  add_index "team_player_searches", ["sport_type_id"], :name => "index_team_player_searches_on_sport_type_id"

  create_table "users", :force => true do |t|
    t.string   "email",                    :default => "", :null => false
    t.string   "encrypted_password",       :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "fname"
    t.string   "lname"
    t.string   "nickname"
    t.boolean  "sex"
    t.date     "birthday"
    t.string   "email2"
    t.string   "phone"
    t.text     "about"
    t.text     "about_sport"
    t.integer  "birthday_day"
    t.integer  "birthday_month"
    t.integer  "birthday_year"
    t.string   "avatar"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer  "place_id"
    t.datetime "online_time"
    t.integer  "permission_send_message"
    t.integer  "permission_view_profile"
    t.integer  "permission_view_friends"
    t.integer  "permission_view_wall"
    t.integer  "permission_view_photo"
    t.integer  "permission_view_video"
    t.integer  "permission_comment_photo"
    t.integer  "permission_comment_video"
    t.integer  "permission_comment_wall"
    t.string   "language"
    t.boolean  "banned"
    t.string   "cover_page"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["place_id"], :name => "index_users_on_place_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_sport_types", :force => true do |t|
    t.integer  "user_id"
    t.integer  "sport_type_id"
    t.integer  "level"
    t.boolean  "search_team"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "users_sport_types", ["sport_type_id"], :name => "index_users_sport_types_on_sport_type_id"
  add_index "users_sport_types", ["user_id"], :name => "index_users_sport_types_on_user_id"

  create_table "videoalbums", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "videoalbumable_id"
    t.string   "videoalbumable_type"
  end

  add_index "videoalbums", ["videoalbumable_id", "videoalbumable_type"], :name => "videoalbumable"

  create_table "videos", :force => true do |t|
    t.integer  "videoalbum_id"
    t.datetime "date"
    t.string   "provider"
    t.string   "video"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "videos", ["user_id"], :name => "index_videos_on_user_id"
  add_index "videos", ["videoalbum_id"], :name => "index_videos_on_videoalbum_id"

end
