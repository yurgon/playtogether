class CreateSportTypes < ActiveRecord::Migration
  def change
    create_table :sport_types do |t|
      t.integer :name
      t.integer :parent_id
      t.integer :lft
      t.integer :rgt
      t.integer :depth

      t.timestamps
    end
  end
end
