class CreateNotices < ActiveRecord::Migration
  def change
    create_table :notices do |t|
      t.string :theme
      t.integer :user_id
      t.text :text
      t.string :type

      t.timestamps
    end
  end
end
