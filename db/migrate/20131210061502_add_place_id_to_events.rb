class AddPlaceIdToEvents < ActiveRecord::Migration
  def change
    add_column :events, :place_id, :integer
    add_column :events, :spot_id, :integer
  end
end
