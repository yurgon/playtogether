class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.integer :photoalbum_id
      t.datetime :date
      t.string :photo
      t.text :description
      t.integer :user_id

      t.timestamps
    end
  end
end
