class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.datetime :date
      t.timestamp :kind
      t.text :description
      t.integer :user_id
      t.integer :team1_id
      t.integer :team2_id

      t.timestamps
    end
  end
end
