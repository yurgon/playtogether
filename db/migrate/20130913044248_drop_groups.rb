class DropGroups < ActiveRecord::Migration
  def change
    rename_table :groups, :communities
    rename_column :communities,:team, :type
  end
end
