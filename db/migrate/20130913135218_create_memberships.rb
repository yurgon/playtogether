class CreateMemberships < ActiveRecord::Migration
  def change
    create_table :memberships do |t|
      t.integer :user_id
      t.integer :community_id
      t.integer :user_status
      t.integer :community_status

      t.timestamps
    end
  end
end
