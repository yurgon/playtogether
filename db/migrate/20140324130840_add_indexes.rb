class AddIndexes < ActiveRecord::Migration
  def change

    add_index :sport_types, :parent_id
    add_index :events, :place_id
    add_index :events, :user_id
    add_index :spots, :place_id
    add_index :spots, :user_id
    add_index :communities, [:id, :type]
    add_index :communities, :place_id
    add_index :communities, :sport_type_id
    add_index :community_roles, [:community_id, :user_id], name: 'community_user'
    add_index :community_roles, :user_id
    add_index :community_roles, :community_id
    add_index :event_subscribes, :user_id
    add_index :event_subscribes, :event_id
    add_index :users_sport_types, :user_id
    add_index :users_sport_types, :sport_type_id
    add_index :favorites, [:favoritable_id, :favoritable_type], name: 'favoritable'
    add_index :friends, :user_id
    add_index :friends, :friend_id
    add_index :friends, [:user_id, :friend_id]
    add_index :friends, [:friend_id, :user_id]
    add_index :videos, :user_id
    add_index :videos, :videoalbum_id
    add_index :videoalbums, [:videoalbumable_id, :videoalbumable_type], name: 'videoalbumable'
    add_index :accepted_event_members, :event_id
    add_index :accepted_event_members, :user_id
    add_index :ads, :place_id
    add_index :ads, :user_id
    add_index :invites, :event_id
    add_index :invites, :community_id
    add_index :members_requests, :community_id
    add_index :members_requests, :user_id
    add_index :memberships, :user_id
    add_index :memberships, :community_id
    add_index :memberships, [:community_id, :user_id], name: 'community_user'
    add_index :memberships, [:user_id, :community_id], name: 'user_community'
    add_index :messages, :receiver_id
    add_index :messages, :sender_id
    add_index :notices, [:id, :type]
    add_index :occupations, :user_id
    add_index :occupations, :place_id
    add_index :photos, :user_id
    add_index :photos, :photoalbum_id
    add_index :comments, :user_id
    add_index :comments, [:commentable_id, :commentable_type], name: 'commentable'
    add_index :photoalbums, [:photoalbumable_id, :photoalbumable_type], name: 'photoalbumable'
    add_index :places, :parent_id
    add_index :users, :place_id
  end
end
