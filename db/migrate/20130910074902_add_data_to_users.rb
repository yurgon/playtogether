class AddDataToUsers < ActiveRecord::Migration
  def change
    add_column :users, :fname, :string
    add_column :users, :lname, :string
    add_column :users, :nickname, :string
    add_column :users, :sex, :boolean
    add_column :users, :birthday, :date
    add_column :users, :email2, :string
    add_column :users, :phone, :string
    add_column :users, :about, :text
    add_column :users, :about_sport, :text
  end
end
