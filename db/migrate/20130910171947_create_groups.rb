class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.boolean :team
      t.string :name
      t.text :about

      t.timestamps
    end
  end
end
