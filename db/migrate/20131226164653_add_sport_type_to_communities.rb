class AddSportTypeToCommunities < ActiveRecord::Migration
  def change
    add_column :communities, :sport_type_id, :integer
  end
end
