class AddRetweets < ActiveRecord::Migration
  def change
    add_column :comments, :retweetable_type, :string
    add_column :comments, :retweetable_id, :integer
    add_index :comments, [:retweetable_id, :retweetable_type]
  end
end
