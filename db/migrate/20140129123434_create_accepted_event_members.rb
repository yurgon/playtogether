class CreateAcceptedEventMembers < ActiveRecord::Migration
  def change
    create_table :accepted_event_members do |t|
      t.integer :user_id
      t.integer :team_id
      t.integer :event_id

      t.timestamps
    end
  end
end
