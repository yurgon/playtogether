class CreateVideoalbums < ActiveRecord::Migration
  def change
    create_table :videoalbums do |t|
      t.string :name
      t.integer :user_id
      t.integer :group_id

      t.timestamps
    end
  end
end
