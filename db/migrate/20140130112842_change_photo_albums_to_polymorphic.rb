class ChangePhotoAlbumsToPolymorphic < ActiveRecord::Migration
  def change
    remove_column :photoalbums, :user_id
    remove_column :photoalbums, :community_id
    add_column :photoalbums, :photoalbumable_id, :integer
    add_column :photoalbums, :photoalbumable_type, :string
  end
end
