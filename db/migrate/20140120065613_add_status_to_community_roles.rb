class AddStatusToCommunityRoles < ActiveRecord::Migration
  def change
    add_column :community_roles, :status, :string
  end
end
