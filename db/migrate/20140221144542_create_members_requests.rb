class CreateMembersRequests < ActiveRecord::Migration
  def change
    create_table :members_requests do |t|
      t.integer :user_id
      t.integer :community_id
      t.timestamps
    end
  end
end
