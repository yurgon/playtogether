class ChangeTypeForCommunities < ActiveRecord::Migration
  def change

    change_column :communities, :type, :string

  end
end
