class CreateUsersSportTypes < ActiveRecord::Migration
  def change
    create_table :users_sport_types do |t|
      t.integer :user_id
      t.integer :sport_type_id
      t.integer :level
      t.boolean :search_team

      t.timestamps
    end
  end
end
