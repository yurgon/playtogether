class AddAdminIdToInvites < ActiveRecord::Migration
  def change
    add_column :invites, :admin_id, :integer
  end
end
