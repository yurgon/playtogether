class ChangeKindTypeInEvents < ActiveRecord::Migration
  def up
    change_column :events, :kind, :integer
  end

  def down
    change_column :events, :kind, :integer
  end
end
