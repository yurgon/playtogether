class CreateCommunityRoles < ActiveRecord::Migration
  def change
    create_table :community_roles do |t|
      t.integer :user_id
      t.integer :community_id
      t.string :role

      t.timestamps
    end
  end
end
