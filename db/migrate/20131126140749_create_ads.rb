class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
      t.string :name
      t.integer :kind
      t.text :description
      t.integer :price
      t.integer :place_id
      t.integer :user_id

      t.timestamps
    end
  end
end
