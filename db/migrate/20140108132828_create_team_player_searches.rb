class CreateTeamPlayerSearches < ActiveRecord::Migration
  def change
    create_table :team_player_searches do |t|
      t.belongs_to :community
      t.belongs_to :sport_type
      t.integer :level

      t.timestamps
    end
    add_index :team_player_searches, :community_id
    add_index :team_player_searches, :sport_type_id
  end
end
