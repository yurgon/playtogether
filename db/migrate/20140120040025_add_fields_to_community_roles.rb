class AddFieldsToCommunityRoles < ActiveRecord::Migration
  def change
    add_column :community_roles, :role_description, :string
    add_column :community_roles, :number, :integer
  end
end
