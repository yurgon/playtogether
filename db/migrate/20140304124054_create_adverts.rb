class CreateAdverts < ActiveRecord::Migration
  def change
    create_table :adverts do |t|
      t.text :html
      t.timestamps
    end
  end
end
