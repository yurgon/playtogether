class ChangeTypeOfSportTypeName < ActiveRecord::Migration
  def change

    change_column :sport_types, :name, :string

  end
end
