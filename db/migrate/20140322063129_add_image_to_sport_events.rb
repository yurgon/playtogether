class AddImageToSportEvents < ActiveRecord::Migration
  def change
    add_column :sport_events, :image, :string
  end
end
