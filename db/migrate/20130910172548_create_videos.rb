class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.integer :videoalbum_id
      t.datetime :date
      t.string :provider
      t.string :video
      t.text :description
      t.integer :user_id

      t.timestamps
    end
  end
end
