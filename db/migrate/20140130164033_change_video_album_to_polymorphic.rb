class ChangeVideoAlbumToPolymorphic < ActiveRecord::Migration
  def change
    remove_column :videoalbums, :user_id
    remove_column :videoalbums, :community_id
    add_column :videoalbums, :videoalbumable_id, :integer
    add_column :videoalbums, :videoalbumable_type, :string
  end
end
