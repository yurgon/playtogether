class AddDateToSportEvents < ActiveRecord::Migration
  def change
    add_column :sport_events, :date, :datetime
  end
end
