class RemoveDateFromMessages < ActiveRecord::Migration
  def change
    add_column :messages, :receiver_status, :integer
    add_column :messages, :sender_status, :integer
    remove_column :messages, :date
  end

end
