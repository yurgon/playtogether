class CreateSportEvents < ActiveRecord::Migration
  def change
    create_table :sport_events do |t|
      t.string :header
      t.string :announce
      t.text :content

      t.timestamps
    end
  end
end
