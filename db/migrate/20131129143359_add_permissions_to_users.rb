class AddPermissionsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :permission_send_message, :integer
    add_column :users, :permission_view_profile, :integer
    add_column :users, :permission_view_friends, :integer
    add_column :users, :permission_view_wall, :integer
    add_column :users, :permission_view_photo, :integer
    add_column :users, :permission_view_video, :integer
    add_column :users, :permission_comment_photo, :integer
    add_column :users, :permission_comment_video, :integer
    add_column :users, :permission_comment_wall, :integer
  end
end
