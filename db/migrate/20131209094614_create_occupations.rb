class CreateOccupations < ActiveRecord::Migration
  def change
    create_table :occupations do |t|
      t.string :user_id
      t.string :name
      t.string :description
      t.string :kind
      t.integer :month_start
      t.integer :year_start
      t.integer :month_finish
      t.integer :year_finish
      t.integer :place_id

      t.timestamps
    end
  end
end
