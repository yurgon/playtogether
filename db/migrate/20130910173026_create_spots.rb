class CreateSpots < ActiveRecord::Migration
  def change
    create_table :spots do |t|
      t.string :name
      t.string :kind
      t.string :paid
      t.integer :place_id
      t.text :size
      t.text :about
      t.integer :user_id

      t.timestamps
    end
  end
end
