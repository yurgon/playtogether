require 'test_helper'

class SearchControllerTest < ActionController::TestCase
  test "should get users" do
    get :users
    assert_response :success
  end

  test "should get infogroups" do
    get :infogroups
    assert_response :success
  end

  test "should get teams" do
    get :teams
    assert_response :success
  end

  test "should get photos" do
    get :photos
    assert_response :success
  end

  test "should get videos" do
    get :videos
    assert_response :success
  end

end
